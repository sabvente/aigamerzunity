﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class Platform : MonoBehaviour, Buffable
{

	public float creatureDistance;
	public float yOffset;
	public Creature left, right;
	public Creature jumpingToLeft, jumpingToRight;
	private List<Buff> buffs = new List<Buff>();
	public event Action<Creature> SteppedOn;

	public Creature GetCreature(Side side)
	{
		switch (side)
		{
		case Side.Left:
			return left;
		case Side.Right:
			return right;
		}
		return null;
	}

	/// <summary>
	/// Gets the other creature standing on the platform
	/// </summary>
	/// <returns>Returns null, if there is no other, or the creature is not standing on the platform.</returns>
	public Creature GetOtherCreature(Creature c)
	{
		if (c == left)
			return right;
		else if (c == right)
			return left;
		else
			return null;
	}

	public void JumpTo(Side side, Creature creature)
	{
		switch (side)
		{
		case Side.Left:
			jumpingToLeft = creature;
			break;
		case Side.Right:
			jumpingToRight = creature;
			break;
		default:
			break;
		}
		creature.onSide = Side.Undefined;
	}

	public void StepOn(Side side, Creature creature)
	{
		creature.CurrentPlatform = this;
		switch (side)
		{
		case Side.Left:
			left = creature;
			creature.onSide = Platform.Side.Left;
			jumpingToLeft = null;
			break;
		case Side.Right:
			right = creature;
			creature.onSide = Platform.Side.Right;
			jumpingToRight = null;
			break;
		}
		FireSteppedOn(creature);
	}

	public void Leave(Creature creature)
	{
		if (left == creature || jumpingToLeft == creature)
		{
			left = null;
			jumpingToLeft = null;
		}
		else if (right == creature || jumpingToRight == creature)
		{
			right = null;
			jumpingToRight = null;
		}
		creature.CurrentPlatform = null;
	}

	public Vector3 GetPosition(Side side)
	{
		switch (side)
		{
		case Side.Left:
			return transform.position - Vector3.right * creatureDistance + Vector3.up * yOffset;
		case Side.Right:
			return transform.position + Vector3.right * creatureDistance + Vector3.up * yOffset;
		default:
			break;
		}
		return Vector3.zero;
	}

	public Side? GetFreePosition()
	{
		if (left == null && !jumpingToLeft)
			return Side.Left;
		else if (right == null && !jumpingToRight)
			return Side.Right;
		else
			return null;
	}

	public enum Side
	{
		Left,
		Right,
		Undefined
	}

	public void AddBuff(Buff buff)
	{
		buffs.Add(buff);
	}

	public void RemoveBuff(Buff buff)
	{
		buffs.Remove(buff);
	}

	public void FireSteppedOn(Creature creature)
	{
		if (SteppedOn != null)
		{
			SteppedOn(creature);
		}
	}
}
