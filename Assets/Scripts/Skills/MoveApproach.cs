using System;
using UnityEngine;

public class MoveApproach : Skill
{
	private Creature enemy;
	protected override void Start()
	{
		base.Start();
		cooldownTime = 0.3f; // TODO calculate speed
		castTime = 0.4f;
		Name = "Approach";
		Supertype = SkillSuperType.Move;
		Subtype = SkillSubType.Approach;
		enemy = Owner.World.GetOtherCreature(Owner);

        // DemoAI
        aggrLevel = 0.6f;
        period = 3;
    }

    public override float sense(DemoAI contr)
    {
        // more farther the better
        float multiplier = 0.8f;
        if (contr.creature.Team == Creature.Teams.Citadel) multiplier = 1;
        return (float)Math.Exp(-Math.Abs((49f - Vector2.Distance(contr.creature.transform.position, contr.otherAI.creature.transform.position)) / 49f) * multiplier);
	}
	
	public override void overwrittenFire(Controller controller)
	{
        if (enemy.CurrentPlatform == null || (controller.creature.CurrentPlatform != null && enemy.CurrentPlatform != null
            && controller.creature.CurrentPlatform.gameObject != enemy.CurrentPlatform.gameObject))
            controller.Move((enemy.transform.position - controller.gameObject.transform.position).normalized);
	}

	public override void applyToState (ref GameState state, int pid)
	{
        var opid = GameState.otherPlayerId(pid);
		Vector2 to = (state.players[opid].pos.transform.position - state.players[pid].pos.transform.position).normalized; // TODO nullref exep
		var newplatform = Controller.PlatformInDir(state.players[pid].pos, to);
		if (newplatform != null && state.players[pid].pos != state.players[opid].pos)
		{
			var pstate = state.players[pid];
			pstate.pos = newplatform;
			state.players[pid] = pstate;
		}
	}
}

