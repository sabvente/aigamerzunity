using System;
using UnityEngine;

public class MoveDirected : Skill
{
	public Vector2 dir = new Vector2();
	protected override void Start()
	{
		base.Start();
		cooldownTime = 0.3f; // TODO calculate speed
		castTime = 0.3f;
		Name = "Move Directed";
		Supertype = SkillSuperType.Move;
		Subtype = SkillSubType.Directed;
	}
	
	public override void overwrittenFire(Controller controller)
	{
		if (CanFire() && Owner.CurrentPlatform != null)
		{
			controller.Move(dir);
			base.Fire(controller);
		}
	}
}
