﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonteCarloController : Controller {

    MonteCarlo mc;

	public override void Start () {
        base.Start();
        
        var creatures = new List<Creature>(2);
        creatures.Add(creature);
        creatures.Add(GameObject.FindObjectOfType<World>().GetOtherCreature(creature));
        // TODO get first param intelligence
        mc = new MonteCarlo(1f, creatures);
	}
	
    public override void Update()
    {
        base.Update();

        if (sequence.Count == 0)
        {
            var tofire = mc.takeOverControl();
            if (tofire != null)
            {
                EnqueueSequence(tofire);
            }
        }
	}
}
