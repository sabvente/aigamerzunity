﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
	public Spline trajectory;
	public bool fadeOutAfterHit = true;
	private float lastPointTime;
	public Creature owner, enemy;
	public float startTime;
	public float hitTime;
	public bool move;
	BoxCollider2D meBBox, enemyBBox;
	public int damage = 30;
	public void Start()
	{
		startTime = Time.time;
		meBBox = gameObject.GetComponent<BoxCollider2D>();
		enemyBBox = enemy.gameObject.GetComponent<BoxCollider2D>();
		gameObject.renderer.enabled = false;
		move = true;
		hitTime = 0;
		lastPointTime = 0;
		foreach (var i in trajectory.arrivalTimes) {
			if(i > lastPointTime) {
				lastPointTime = i;
			}
		}
	}

	public void Update()
	{
		if (startTime <= Time.time)
		{
			gameObject.renderer.enabled = true;
			if (move) gameObject.transform.position = trajectory.calculatePosition(Time.time - startTime);
			else
			{
				SpriteRenderer s = (SpriteRenderer)gameObject.GetComponent<SpriteRenderer>();
				float transparency = 0.5f - Time.time + hitTime;
				if (transparency <= 0) Destroy(gameObject);
				s.color = new Color(1f, 1f, 1f, transparency * 2);
			}

			if (Time.time - startTime >= lastPointTime)
			{
				Destroy(gameObject);
			}

			if (enemy != null && meBBox.bounds.Intersects(enemyBBox.bounds) && move)
			{
				enemy.TakeDamage(damage, owner);
				hitTime = Time.time;
				if(!fadeOutAfterHit) Destroy(gameObject);
				move = false;
			}
		}
	}
}