using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Reflection;

public class SoftAIController : PlayerController
{
	private GameObject enemyGO;
	private GameObject startPlatform;
	private SoftAI AI;
	
	public override void Start()
	{
		base.Start();

		var eventMacroGO = GameObject.Find("EventMacro");
		if (eventMacroGO == null)
			return;

		enemyGO = creature.World.GetOtherCreature(creature).gameObject;
		startPlatform = creature.CurrentPlatform.gameObject;
		
		AI = new SoftAI(this, enemyGO.GetComponent<Controller>());
		var eventMacroMapping = eventMacroGO.GetComponent<EventMacro>().macros;
		
		foreach (var emMap in eventMacroMapping)
		{
			if (!AI.macroTable.ContainsKey(emMap.Key.SuperType))
			{
				AI.macroTable.Add(emMap.Key.SuperType, new Dictionary<SkillSubType, Macro>());
			}
			
			var skillList = new List<Skill>();
			foreach (var skillName in emMap.Value)
			{
				if (skillName != null)
				{
					Skill skill = (Skill)creature.gameObject.GetComponent(skillName);
					//if not found add it
					if (skill == null)
						skill = (Skill)(creature.gameObject.AddComponent(skillName));
					if (skill == null)
						Debug.LogError("Can't find skill: " + skillName);
					skillList.Add(skill);
					Debug.Log("SKILL LOADED: " + skillName);
				}
			}
			
			if (skillList.Count > 0)
			{
				var macro = creature.gameObject.AddComponent<Macro>();
				macro.skills = skillList;
				macro.Name = emMap.Key.SkillName;
				AI.macroTable[emMap.Key.SuperType].Add(emMap.Key.SubType, macro);
			}
		}
		
		// MANUAL
		/*var attackList = new List<Skill>();
		attackList.Add (new MeleeAttack(this.creature));
		attackList.Add (new MoveDodge(this.creature));
		var attackMacro = new Macro(attackList);
		var attackDic = new Dictionary<string, Macro> ();
		attackDic.Add ("default", attackMacro);
		
		var skillList = new List<Skill>();
		skillList.Add(new MoveApproach(this.creature));
		skillList.Add(new MoveApproach(this.creature));
		var moveApprMacro = new Macro(skillList);
		
		var moveList = new List<Skill>();
		moveList.Add(new MoveApproach(this.creature));
		var moveDefMacro = new Macro(moveList);
		
		var moveDic = new Dictionary<string, Macro> ();
		moveDic.Add("Approach", moveApprMacro);
		moveDic.Add("default", moveDefMacro);
		
		AI.macroTable.Add("Attack", attackDic);
		AI.macroTable.Add("Move", moveDic);*/
	}
}
