﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SliderAI : MonoBehaviour
{

	public GameObject pointsLeftGO, pointsRightGO;
	public GameObject agressivityLeftGO, agressivityRightGO;
	public GameObject learnLeftGO, learnRightGO;
	public GameObject foresightLeftGO, foresightRightGO;

	private int pointsLeft, pointsRight;
	private int agressivityLeft, agressivityRight;
	private int learnLeft, learnRight;
	private int foresightLeft, foresightRight;

	private Text pointsLeftText, pointsRightText;

	private static readonly int fullPoints = 15;

	public void Start()
	{
		pointsLeft = 3;
		pointsRight = 3;
		agressivityLeft = 4;
		agressivityRight = 4;
		learnLeft = 4;
		learnRight = 4;
		foresightLeft = 4;
		foresightRight = 4;

		pointsLeftText = pointsLeftGO.GetComponent<Text>();
		pointsRightText = pointsRightGO.GetComponent<Text>();


		Slider slider1 = agressivityLeftGO.GetComponent<Slider>();
		Text text1 = slider1.gameObject.transform.Find("Handle Slide Area/Handle/Text").GetComponent<Text>();
		slider1.onValueChanged.AddListener((value) =>
		{
			CalcVals(ref agressivityLeft, ref pointsLeft, (int)value);
			UpdateValues(text1, slider1, agressivityLeft, pointsLeftText, pointsLeft);
		});

		Slider slider2 = learnLeftGO.GetComponent<Slider>();
		Text text2 = slider2.gameObject.transform.Find("Handle Slide Area/Handle/Text").GetComponent<Text>();
		slider2.onValueChanged.AddListener((value) =>
		{
			CalcVals(ref learnLeft, ref pointsLeft, (int)value);
			UpdateValues(text2, slider2, learnLeft, pointsLeftText, pointsLeft);
		});

		Slider slider3 = foresightLeftGO.GetComponent<Slider>();
		Text text3 = slider3.gameObject.transform.Find("Handle Slide Area/Handle/Text").GetComponent<Text>();
		slider3.onValueChanged.AddListener((value) =>
		{
			CalcVals(ref foresightLeft, ref pointsLeft, (int)value);
			UpdateValues(text3, slider3, foresightLeft, pointsLeftText, pointsLeft);
		});



		Slider slider4 = agressivityRightGO.GetComponent<Slider>();
		Text text4 = slider4.gameObject.transform.Find("Handle Slide Area/Handle/Text").GetComponent<Text>();
		slider4.onValueChanged.AddListener((value) =>
		{
			CalcVals(ref agressivityRight, ref pointsRight, (int)value);
			UpdateValues(text4, slider4, agressivityRight, pointsRightText, pointsRight);
		});

		Slider slider5 = learnRightGO.GetComponent<Slider>();
		Text text5 = slider5.gameObject.transform.Find("Handle Slide Area/Handle/Text").GetComponent<Text>();
		slider5.onValueChanged.AddListener((value) =>
		{
			CalcVals(ref learnRight, ref pointsRight, (int)value);
			UpdateValues(text5, slider5, learnRight, pointsRightText, pointsRight);
		});

		Slider slider6 = foresightRightGO.GetComponent<Slider>();
		Text text6 = slider6.gameObject.transform.Find("Handle Slide Area/Handle/Text").GetComponent<Text>();
		slider6.onValueChanged.AddListener((value) =>
		{
			CalcVals(ref foresightRight, ref pointsRight, (int)value);
			UpdateValues(text6, slider6, foresightRight, pointsRightText, pointsRight);
		});


		UpdateValues(text1, slider1, agressivityLeft, pointsLeftText, pointsLeft);
		UpdateValues(text2, slider2, learnLeft, pointsLeftText, pointsLeft);
		UpdateValues(text3, slider3, foresightLeft, pointsLeftText, pointsLeft);
		UpdateValues(text4, slider4, agressivityRight, pointsRightText, pointsRight);
		UpdateValues(text5, slider5, learnRight, pointsRightText, pointsRight);
		UpdateValues(text6, slider6, foresightRight, pointsRightText, pointsRight);
	}

	private void CalcVals(ref int AIparam, ref int points, int newValue)
	{
		int delta = newValue - AIparam;
		if (points - delta < 0)
		{
			delta = points;
			points = 0;
		}
		//else if (points - delta > fullPoints)
		//{
		//	delta = fullPoints - points;
		//	pointsLeft = fullPoints;
		//}
		else
			points -= delta;

		AIparam += delta;
	}

	public void UpdateValues(Text text, Slider slider, int val, Text pointsText, int points)
	{
		if (text != null)
			text.text = val.ToString();
		slider.value = val;
		string s = points.ToString();
		if (!pointsText.text.Equals(s))
		{
			if(points == 0)
				StartCoroutine(playPuffZero(pointsText));
			pointsText.text = s;
		}
	}

	private IEnumerator playPuffZero(Text t)
	{
		int origSize = t.fontSize;
		for (int i = 0; i < 20; i++)
		{
			t.fontSize = origSize + i;
			yield return new WaitForEndOfFrame();
		}
		for (int i = 19; i >= 0; i--)
		{
			t.fontSize = origSize + i;
			yield return new WaitForEndOfFrame();
		}
	}

	public void Save()
	{
		GameObject go = new GameObject("SliderAIParams");
		var sAI = go.AddComponent<SliderAIParams>();

		sAI.agressivityLeft = agressivityLeft;
		sAI.agressivityRight = agressivityRight;

		sAI.learnLeft = learnLeft;
		sAI.learnRight = learnRight;

		sAI.foresightLeft = foresightLeft;
		sAI.foresightRight = foresightRight;
	}
}
