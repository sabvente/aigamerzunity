using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PoisonedDagger : Skill
{
	public int damage = 5;
	protected override void Start()
	{
		base.Start();
		cooldownTime = 5f;
		castTime = 0.2f;
		Name = "Poisoned spike";
		Supertype = SkillSuperType.Attack;
		Subtype = SkillSubType.Poison;
	}

	public override void overwrittenFire(Controller controller)
	{
		if (CanFire() && Owner.CurrentPlatform != null)
		{
			Owner.isAttacking = true;
			//find enemy
			Creature enemy = Owner.World.GetOtherCreature(Owner);
			if (enemy.CurrentPlatform == Owner.CurrentPlatform)
			{
				enemy.TakeDamage(damage, Owner);
				var p = enemy.gameObject.AddComponent<Poison>();
				p.Cast(enemy, Owner);
			}
			base.Fire(controller);
		}
	}
}