﻿using UnityEngine;
using System.Collections;

public class Defending : Skill
{
	protected override void Start()
	{
		base.Start();
		cooldownTime = 2.5f;
		castTime = 0.1f;
		Name = "Defending";
		Supertype = SkillSuperType.Defense;
		Subtype = SkillSubType.Default;
		IsBasicAttack = false;

        // DemoAI
        aggrLevel = 0;
        period = 4;
    }

    public override float sense(DemoAI contr)
    {
        if (contr.creature.Team == Creature.Teams.Citadel
            && contr.creature.CurrentPlatform != null
            && contr.otherAI.creature.CurrentPlatform != null
            && contr.creature.CurrentPlatform == contr.otherAI.creature.CurrentPlatform)
            return contr.foresight / 1.5f;
        else if (contr.creature.Team == Creature.Teams.Gaia)
            return contr.otherAI.creature.skills[0].sense(contr.otherAI) * contr.foresight / 1.65f; // Citadel SphereBullet sense

        return UnityEngine.Random.Range(0f, 0.3f);
	}

	public override void overwrittenFire(Controller controller)
	{
		// flag causes creature to take less damage -- controlled in Creature.cs
		Owner.isDefending = true;
		Owner.StartCoroutine(addDefense());
	}
		
	public override void applyToState(ref GameState state, int pid)
	{
		// ???
	}
	
	IEnumerator addDefense()
	{
		yield return new WaitForSeconds(cooldownTime);
		Owner.isDefending = false;
	}
}