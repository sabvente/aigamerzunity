﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class WildRoarBuff : Buff
{
	public int defense = 10;

	Defense targetDefense;
	public WildRoarBuff()
	{
		Name = "Wild roar";
		Supertype = SkillSuperType.Defense;
		Subtype = SkillSubType.Default;
		BuffTime = 20;
	}

	public override void Cast(Buffable target, Creature caster = null)
	{
		Creature c = target as Creature;
		targetDefense = (Defense)(c.buffs.Find(ps => ps is Defense));
		targetDefense.defense += defense;
		base.Cast(target, caster);
	}

	public override void DeCast()
	{
		targetDefense.defense -= defense;
		base.DeCast();
	}
}
