﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class DemoTeachGesture : MonoBehaviour
{
	public GameObject particleGO;
	public GameObject particleFullGO;
	public GameObject particleSmallGO;

	private RawGestureTemplates rawGestureTemplates;
	private List<GestureTouch> learnedGesture = new List<GestureTouch>();
	private List<float> touchDistances = new List<float>();
	private ParticleSystem _particleSystem;
	private ParticleSystem _particleSystemFull;
	private ParticleSystem _particleSystemSmall;
	private Vector3 prevMousePosition;
	private bool isDrawing;

	void Start()
	{
		rawGestureTemplates = GameObject.Find("RawGestureTemplates").GetComponent<RawGestureTemplates>();

		//set up recording
		Input.simulateMouseWithTouches = false;
		prevMousePosition = Input.mousePosition;

		_particleSystemFull = GameObject.Find("SkillParticleFull").GetComponent<ParticleSystem>();
		//StartCoroutine(PullSmallParticles()); - fuckin slow
		var pGO = (GameObject)Instantiate(particleSmallGO);
		_particleSystemSmall = pGO.particleSystem;

		StartCoroutine(EmitSmallParticles());
	}

	private IEnumerator EmitSmallParticles()
	{
		while (true)
		{
			yield return new WaitForSeconds(0.1f);
			if (learnedGesture.Count == 0)
				continue;
			int i = Random.Range(0, learnedGesture.Count-1);
			Vector3 velocity = Random.insideUnitSphere * 10;
			velocity.z = 0;
			_particleSystemSmall.Emit(learnedGesture[i].position + Vector3.forward * 20f,
				velocity, 5, 5, new Color32(123, 212, 255, 255));
		}
	}

	private IEnumerator PullSmallParticles()
	{
		while (true)
		{
			yield return new WaitForSeconds(1f);
			if (_particleSystem == null)
				continue;
			var smallParticles = new ParticleSystem.Particle[_particleSystemFull.particleCount];
			_particleSystemFull.GetParticles(smallParticles);
			var particles = new ParticleSystem.Particle[_particleSystem.particleCount];
			_particleSystem.GetParticles(particles);

			ParticleSystem.Particle? closest;
			float closestDist;
			for (int i = 0; i < smallParticles.Length; i++)
			{
				smallParticles[i].velocity = Vector3.zero;
				int j = 0;
				closest = null;
				closestDist = float.MaxValue;
				foreach (var p in particles)
				{
					if (j++ % 20 != 0)
						continue;
					float d = Vector3.Distance(smallParticles[i].position, p.position);

					if (d < closestDist && d < 40 && d > 10)
					{
						closest = p;
						closestDist = d;
					}
				}
				if (closest != null)
				{
					smallParticles[i].velocity = (closest.Value.position - smallParticles[i].position).normalized*2;
				}
			}
			_particleSystemFull.SetParticles(smallParticles, smallParticles.Length);
		}
	}

	void Update()
	{
		bool posCorrected = false;
		GestureTouch gestureTouch;
		if (Input.touches.Length > 0)
		{
			var touch = Input.touches[0];
			gestureTouch = new GestureTouch(Camera.main.ScreenToWorldPoint(touch.position), Vector3.zero);

			if (touch.phase == TouchPhase.Began)
			{
				if (!posCorrected)
				{
					if (_particleSystem != null)
						GameObject.Destroy(_particleSystem.gameObject);
					var pGO = (GameObject)Instantiate(particleGO, gestureTouch.position + Vector3.forward * 20f, Quaternion.identity);
					_particleSystem = pGO.particleSystem;
					_particleSystem.simulationSpace = ParticleSystemSimulationSpace.World;
					touchDistances.Clear();
					learnedGesture.Clear();
					isDrawing = true;
				}
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				if (isDrawing)
				{
					learnedGesture.Add(gestureTouch);
					if (learnedGesture.Count > 1)
					{
						touchDistances.Add(Vector3.Distance(
							learnedGesture[learnedGesture.Count - 2].position,
							learnedGesture[learnedGesture.Count - 1].position));
					}
					UpdateParticles();
				}
			}
			else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
			{
				if (isDrawing)
				{
					isDrawing = false;
					learnedGesture.Add(gestureTouch);
					if (learnedGesture.Count > 1)
					{
						touchDistances.Add(Vector3.Distance(
							learnedGesture[learnedGesture.Count - 2].position,
							learnedGesture[learnedGesture.Count - 1].position));
					}
					UpdateParticles();
				}
			}
		}
		gestureTouch = new GestureTouch(Camera.main.ScreenToWorldPoint(Input.mousePosition),
			prevMousePosition - Input.mousePosition);

		if (Input.GetMouseButtonDown(0))
		{
			if (!posCorrected)
			{
				if (_particleSystem != null)
					GameObject.Destroy(_particleSystem.gameObject);
				var pGO = (GameObject)Instantiate(particleGO, gestureTouch.position + Vector3.forward * 20f, Quaternion.identity);
				_particleSystem = pGO.particleSystem;
				_particleSystem.simulationSpace = ParticleSystemSimulationSpace.World;
				touchDistances.Clear();
				learnedGesture.Clear();
				isDrawing = true;
			}
		}
		if (Input.GetMouseButton(0))
		{
			if (isDrawing)
			{
				learnedGesture.Add(gestureTouch);
				if (learnedGesture.Count > 1)
				{
					touchDistances.Add(Vector3.Distance(
						learnedGesture[learnedGesture.Count - 2].position,
						learnedGesture[learnedGesture.Count - 1].position));
				}
				UpdateParticles();
			}
		}
		if (Input.GetMouseButtonUp(0))
		{
			if (isDrawing)
			{
				isDrawing = false;
				learnedGesture.Add(gestureTouch);
				if (learnedGesture.Count > 1)
				{
					touchDistances.Add(Vector3.Distance(
						learnedGesture[learnedGesture.Count - 2].position,
						learnedGesture[learnedGesture.Count - 1].position));
				}
				UpdateParticles();
			}
		}
		prevMousePosition = Input.mousePosition;
	}

	private void UpdateParticles()
	{
		if (learnedGesture.Count > 1)
		{
			//the whole gesture line length
			float length = 0;
			foreach (var item in touchDistances)
				length += item;
			//the distance between any two particle next to each other
			float segmentLength = 2.5f;
			//count of particles to draw
			int segmentCount = (int)(length / segmentLength) + 1;

			float remainder = 0;
			int j = 0;
			var particles = new ParticleSystem.Particle[segmentCount + 1];
			//add the very first particle
			AddParticle(particles, learnedGesture[0].position + Vector3.forward * 20f, j++);

			for (int i = 0; i < touchDistances.Count; i++)
			{
				//if we need to add particles in this touch
				if (remainder + touchDistances[i] > segmentLength)
				{
					var nextTouchPos = learnedGesture[i + 1].position + Vector3.forward * 20f;
					//add as much particle as we can between the two touch
					// x-----x-----x-----x-----x-----x--- particles
					// o---------------------------o----- touches
					while (remainder + touchDistances[i] > segmentLength)
					{
						var particlePos = particles[j - 1].position + (nextTouchPos - particles[j - 1].position).normalized * segmentLength;
						AddParticle(particles, particlePos, j++);
						remainder -= segmentLength;
					}

					//remainder from this segment
					//       | rem. |
					// x-----x-----x--- particles
					// o---------o----- touches
					remainder = remainder + touchDistances[i];
				}
				// if the distance is too short
				// x--------x---- particles
				// o--o--o--o--o- touches
				else
				{
					remainder += touchDistances[i];
				}
			}
			_particleSystem.SetParticles(particles, particles.Length);
		}
	}

	private void AddParticle(ParticleSystem.Particle[] particles, Vector3 position, int index)
	{
		var p = new ParticleSystem.Particle();
		var color = new Color(0.3411f, 0.5882f, 0.9294f, 0.25f);
		p.color = color;
		p.position = position;
		p.startLifetime = float.MaxValue;
		p.lifetime = float.MaxValue;
		p.size = 20;
		particles[index] = p;
	}

	/// <summary>
	/// Corrects position to be in the bounds
	/// </summary>
	bool CorrectPos(Vector3 pos, out Vector3 newPos)
	{
		newPos = pos;
		return false;
	}

	public void Save()
	{
		rawGestureTemplates.templates.Add(
			new GestureTemplate("", 1, GestureSegment.createTemplateSegments(learnedGesture)));
	}
}
