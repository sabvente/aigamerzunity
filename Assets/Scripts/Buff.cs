﻿using UnityEngine;
using System.Collections;

public abstract class Buff : MonoBehaviour
{
	public string Name { get; set; }
	public SkillSuperType Supertype { get; set; }
	public SkillSubType Subtype { get; set; }
	public float? BuffTime { get; set; }
	public Buffable Target { get; set; }
	public virtual void Cast(Buffable target, Creature caster = null)
	{
		Target = target;
		Target.AddBuff(this);
		if (BuffTime.HasValue)
		{
			StartCoroutine(CoolDownTimer());
		}
	}
	public virtual void DeCast()
	{
		Target.RemoveBuff(this);
		Destroy(this);
	}

	IEnumerator CoolDownTimer()
	{
		yield return new WaitForSeconds(BuffTime.Value);
		DeCast();
	}

	public virtual int GetDamage(int damage) { return damage; }
}
