﻿using UnityEngine;
using System.Collections;

public class HPBar : MonoBehaviour
{
	Creature creature;
	RectTransform HPLeft, HPRight;

	void Start()
	{
		creature = GetComponent<Creature>();
		var HPLeftGO = GameObject.Find("HPLeft");
		if (HPLeftGO != null)
			HPLeft = HPLeftGO.GetComponent<RectTransform>();
		var HPRightGO = GameObject.Find("HPRight");
		if (HPRightGO != null)
			HPRight = HPRightGO.GetComponent<RectTransform>();
	}

	void Update()
	{
		if (HPLeft == null || HPRight == null)
			return;
		float a = creature.Health / (float)creature.DefaultHealth;
		if (creature.isLeftHUD)
		{
			HPLeft.offsetMax = new Vector2(HPLeft.offsetMax.x, (1 - a) * (-HPLeft.rect.height / 2));
			HPLeft.offsetMin = new Vector2(HPLeft.offsetMin.x, (1 - a) * (-HPLeft.rect.height / 2));
		}
		else
		{
			HPRight.offsetMax = new Vector2(HPRight.offsetMax.x, (1 - a) * (-HPRight.rect.height / 2));
			HPRight.offsetMin = new Vector2(HPRight.offsetMin.x, (1 - a) * (-HPRight.rect.height / 2));
		}
	}
}
