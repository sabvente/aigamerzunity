﻿using UnityEngine;
using System.Collections;

public class SkillStore : MonoBehaviour {

	public SkillSpriteEntry[] skills;

	[System.Serializable]
	public class SkillSpriteEntry
	{
		public string skillName;
		public Sprite sprite;
	}
}
