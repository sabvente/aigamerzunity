﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class LearnScenario : Scenario
{
    protected float learnt = 1;
    protected List<int> learnContrSeq = new List<int>();
    protected List<int> learnEnemySeq = new List<int>();

    public override float applicability(DemoAI contr) { return 0; }
    public override void apply(DemoAI contr)
    {
        base.apply(contr);
        learnt -= contr.learnspeed / 1.5f;
		contr.world.AIText("Learning phase", contr.world.myCreature == contr.creature);
    }
}
