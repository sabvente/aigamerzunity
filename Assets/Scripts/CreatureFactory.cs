﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CreatureFactory : MonoBehaviour
{
	// Set by the editor
	public GameObject[] creaturePrefabs;

	public Creature SpawnCreature(string creatureName, Vector3 position)
	{
		var prefab = creaturePrefabs.ToList().Find(pf =>
		{
			var c = pf.GetComponent<Creature>();
			return c.CreatureName == creatureName;
		});

		var creatureGO = (GameObject)Instantiate(prefab, position, Quaternion.identity);

		var creature = creatureGO.GetComponent<Creature>();
		AddSkillsAndBuffs(creature);
		return creature;
	}

	public Creature SpawnCreature(string creatureName, Platform platform, Platform.Side side, bool isLeftHUD)
	{
		var position = platform.GetPosition(side);
		Creature creature = SpawnCreature(creatureName, position);
		creature.isLeftHUD = isLeftHUD;
		platform.StepOn(side, creature);
		return creature;
	}

	public void AddSkillsAndBuffs(Creature creature)
	{
		foreach (var bName in GetBuffs(creature.CreatureName))
		{
			var buff = (Buff)creature.gameObject.AddComponent(bName);
			if (buff == null)
				Debug.LogError("Can't find buff: " + bName);
			else
				buff.Cast(creature);
		}
		foreach (var sName in GetSkills(creature.CreatureName))
		{
			var skill = (Skill)creature.gameObject.AddComponent(sName);
			if (skill == null)
				Debug.LogError("Can't find skill: " + sName);
			else
				creature.skills.Add(skill);
		}
		//creature.gameObject.AddComponent<Idle>();
	}

	public List<string> GetBuffs(string creatureName)
	{
		var list = new List<string>();
		switch (creatureName)
		{
		case "BlackBear":
			list.Add("Defense");
			break;
		case "Igor":
			break;
		default:
			break;
		}
		return list;
	}

	public List<string> GetSkills(string creatureName)
	{
		var list = new List<string>();
		switch (creatureName)
		{
		case "BlackBear":
			list.Add("MeleeAttack");
			list.Add("LeapAttack");
			list.Add("WildRoar");
			break;
		case "Igor":
			list.Add("VialThrow");
			list.Add("UseHealthPotion");
			list.Add("PoisonedDagger");
			break;
		case "Green":
            list.Add("SphereBullet");
            list.Add("Defending");
            list.Add("MoveApproach");
            list.Add("MoveDodge");
            list.Add("MoveWithdraw");
            list.Add("Idle");
			break;
		case "Red":
			list.Add("MeleeAttack");
            list.Add("Defending");
            list.Add("MoveApproach");
            list.Add("MoveDodge");
            list.Add("MoveWithdraw");
            list.Add("Idle");
			break;
		default:
			break;
		}
		return list;
	}

	public List<string> GetGestureSkills(string creatureName)
	{
		var list = GetSkills(creatureName);
		list.RemoveAll(s => s == "MeleeAttack" || s == "VialThrow");
		return list;
	}

	public List<string> GetSoftAISkills(string creatureName)
	{
		var list = GetSkills(creatureName);
		list.Add("MoveApproach");
		list.Add("MoveDodge");
		list.Add("MoveWithdraw");
		return list;
	}

}
