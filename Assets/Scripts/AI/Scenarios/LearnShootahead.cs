﻿using UnityEngine;
using System.Collections;

public class LearnShootAhead : LearnScenario // TODO not always working
{
    public LearnShootAhead()
    {
        // TODO fill learnContrSeq
        learnContrSeq.Add(0);
        // TODO fill learnEnemySeq
        learnEnemySeq.Add(5);
        learnEnemySeq.Add(3);
        // TODO fill contrSeq
        contrSeq.Add(0);
        // TODO fill enemySeq
        enemySeq.Add(5);
        enemySeq.Add(3);
        enemySeq.Add(5);
        enemySeq.Add(5);
        // TODO fill freq?
        textShowDur = 3f;
    }

    public override float applicability(DemoAI contr)
    {
        var dist = Scenario.stepDistance(contr);
        if (contr.creature.Team == Creature.Teams.Citadel && dist >= 2 && dist < 4
            && seqApplicable(contr, contrSeq) == 0f)
            return (Time.time - appliedSince) / period * contr.agressivity;
		return 0;
    }

    public override void apply(DemoAI contr)
    {
        if (applicability(contr) <= 0) return;
        base.apply(contr);

        var ctodo = contrSeq;
        var etodo = enemySeq;
        if (learnt > 0) // learn
        {
            ctodo = learnContrSeq;
            etodo = learnEnemySeq;
            contr.world.AIText("Learns to shoot ahead", contr.world.myCreature == contr.creature, textShowDur);
		}
		else
		{
			var vec = contr.otherAI.creature.transform.position - contr.creature.transform.position;
			Platform towards = Controller.PlatformInDir(contr.otherAI.creature.CurrentPlatform, new Vector2(vec.y, -vec.x).normalized);
			if (towards == null)
				towards = Controller.PlatformInDir(contr.otherAI.creature.CurrentPlatform, new Vector2(-vec.y, vec.x).normalized);

            Debug.Log("towards: " + vec);
			((SphereBullet)contr.creature.skills[0]).towards = towards;
            contr.world.AIText("Shoots ahead (learnt)", contr.world.myCreature == contr.creature, textShowDur);
		}

        foreach (var s in ctodo)
            contr.EnqueueSequence(contr.creature.skills[s]);
        foreach (var s in etodo)
            contr.otherAI.EnqueueSequence(contr.otherAI.creature.skills[s]);
    }

}
