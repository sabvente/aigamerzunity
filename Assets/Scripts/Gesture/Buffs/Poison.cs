﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class Poison : Buff
{
	public Poison()
	{
		Name = "Poison";
		Supertype = SkillSuperType.Attack;
		Subtype = SkillSubType.Poison;
		BuffTime = 10;
	}

	public override void Cast(Buffable target, Creature caster = null)
	{
		base.Cast(target);
		Creature targetCreature = target as Creature;
		StartCoroutine(Damage(targetCreature, caster));
	}
	public override void DeCast()
	{
		StopCoroutine(Damage(null, null));
	}

	IEnumerator Damage(Creature target, Creature damager)
	{
		for (int i = 0; i < 10; i++)
		{
			target.TakeDamage(5, damager);
			yield return new WaitForSeconds(1);
		}
	}

}
