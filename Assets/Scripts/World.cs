﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class World : MonoBehaviour
{
	// Set by the editor
	public GameObject gestureParticleSystem;
	public GameObject gestureParticleSystemFull;
	public GameObject gestureWind;
	public CreatureFactory creatureFactory;
	public GameObject gameOverPanel;
	public GameObject camera;
	public GameObject overlay;
	public GameObject skillsPanel;
	public GameObject skillsPanelSkill;
	public GameObject firedSkillIconGO;
	public GameObject skillStoreGO;
	public GameObject canvas;

	bool isGameOver;
	[HideInInspector]
	public Creature myCreature;
	[HideInInspector]
	public Creature enemyCreature;
	[HideInInspector]
	public bool IsSkillMode = false;
	[HideInInspector]
	public SkillStore skillStore;

    public MonteCarlo mc;

	private GameObject _particleSystemFull;
	private GameObject _overlay;

	private Text AILeft, AIRight;

	void Start()
	{
		// Creature selection is currently ignored.
		/*var cs = GameObject.Find("SelectedCreature");
		if (cs != null)
			myCreatureName = cs.GetComponent<SelectedCreature>().selected;*/

		// Creature selection should only give values to these variables and the AI controller related ones.
		var myIsGaia = false;
		var myCreatureName = "Green";
		var enemyCreatureName = "Red";

		AudioManager.initialize(myCreatureName, enemyCreatureName);

		var myPlatform = GameObject.Find("MyStartPlatform").GetComponent<Platform>();
		myCreature = creatureFactory.SpawnCreature(myCreatureName, myPlatform, Platform.Side.Left, !myIsGaia);
		GameObject.Find("LeftName").GetComponent<Text>().text = myCreature.CreatureName;
		
		var enemyPlatform = GameObject.Find("EnemyStartPlatform").GetComponent<Platform>();
		enemyCreature = creatureFactory.SpawnCreature(enemyCreatureName, enemyPlatform, Platform.Side.Right, myIsGaia);
		GameObject.Find("RightName").GetComponent<Text>().text = enemyCreature.CreatureName;

		var sliderAIparams = GameObject.FindObjectOfType<SliderAIParams>();

		// Adding controllers.
        var pc = enemyCreature.gameObject.AddComponent<DemoAI>();
		pc.agressivity = sliderAIparams.agressivityRight / 11.0f;
		pc.learnspeed = sliderAIparams.learnRight / 11.0f;
		pc.foresight = sliderAIparams.foresightRight / 11.0f;
		var ec = myCreature.gameObject.AddComponent<DemoAI>();
		ec.agressivity = sliderAIparams.agressivityLeft / 11.0f;
		ec.learnspeed = sliderAIparams.learnLeft / 11.0f;
		ec.foresight = sliderAIparams.foresightLeft / 11.0f;
        pc.otherAI = ec;
        ec.otherAI = pc;

		//enemyCreature.gameObject.AddComponent<StupidAI>();

		// Adding Levi's fancyness :D
		//pc.gestureParticleSystem = gestureParticleSystem;
		//pc.gestureWind = gestureWind;

		//add skill icons to skill mode
		var ssGO = (GameObject)Instantiate(skillStoreGO);
		skillStore = ssGO.GetComponent<SkillStore>();
		var skillNames = creatureFactory.GetGestureSkills(myCreatureName);
		int addedSkills = 0;
		foreach (var skillSprite in skillStore.skills)
		{
			if (skillNames.Contains(skillSprite.skillName))
			{
				string skillName = skillSprite.skillName;
				var skillIconClone = (GameObject)Instantiate(skillsPanelSkill);
				skillIconClone.transform.SetParent(skillsPanel.transform, false);
				//set sprite
				var img = skillIconClone.transform.Find("Image").GetComponent<Image>();
				img.sprite = skillSprite.sprite;
				//set skill name
				var text = skillIconClone.transform.Find("Text").GetComponent<Text>();
				text.text = skillName;
				//subscribe to CD
				/*Skill s = (Skill)myCreature.GetComponent(skillSprite.skillName);
				s.CanFireEvent += () =>
				{
					img.color = new Color(1, 1, 1);
					text.color = new Color(1, 1, 1);
				};
				s.CannotFireEvent += () =>
				{
					img.color = new Color(1, 1, 1, 0.3f);
					text.color = new Color(1, 1, 1, 0.3f);
				};*/
				addedSkills++;
			}
		}
		skillsPanel.SetActive(false);

		AILeft = GameObject.Find("AILeft").GetComponent<Text>();
		AIRight = GameObject.Find("AIRight").GetComponent<Text>();
	}

	public void AIText(string s, bool isLeft = true, float time = 2f)
	{
		Text t = null;
		if (isLeft)
			t = AILeft;
		else
			t = AIRight;
		StopCoroutine("textFade");
		t.text = s;
		t.color = new Color(t.color.r, t.color.g, t.color.b, 1);
		StartCoroutine(textFade(t));
	}

    private IEnumerator textFade(Text t, float time = 2f)
	{
		yield return new WaitForSeconds(time);
		t.color = new Color(t.color.r, t.color.g, t.color.b, 0);
	}

	public void Killed(Creature killer, Creature killed)
	{
		if (isGameOver)
			return;
		isGameOver = true;
		//disable AIs
		myCreature.GetComponent<Controller>().GameOver();
		enemyCreature.GetComponent<Controller>().GameOver();

		//exit skill mode
		if (IsSkillMode)
			BasicMode();

		//disable controls
		var pc = myCreature.GetComponent<PlayerController>();
		if(pc != null)
			pc.enabled = false; // TODO nullref exep

		//create panel
		var go = (GameObject)Instantiate(gameOverPanel);
		go.transform.parent = GameObject.Find("Canvas").transform;
		var rawImage = go.GetComponent<RawImage>();
		var goPanel = gameOverPanel.GetComponent<GameOverPanel>();

		rawImage.texture = goPanel.victoryGaia.texture;

		StartCoroutine(GoBack());
	}

	private IEnumerator GoBack()
	{
		yield return new WaitForSeconds(2);
		Application.LoadLevel("DemoCraftAI");
	}

	public Creature GetOtherCreature(Creature me)
	{
		if (myCreature == me)
			return enemyCreature;
		else
			return myCreature;
	}

	public void SwitchMode()
	{
		Debug.Log("SwitchMode");
		if (IsSkillMode)
			BasicMode();
		else
			SkillMode();
		IsSkillMode = !IsSkillMode;
	}

	private void SkillMode()
	{
		_particleSystemFull = (GameObject)Instantiate(gestureParticleSystemFull);
		_overlay = (GameObject)Instantiate(overlay);
		skillsPanel.SetActive(true);
	}

	private void BasicMode()
	{
		GameObject.Destroy(_particleSystemFull);
		GameObject.Destroy(_overlay);
		skillsPanel.SetActive(false);
	}

	public void FadeOutSkill(string skillName)
	{
		foreach (var skillSprite in skillStore.skills)
		{
			if (skillSprite.skillName == skillName)
			{
				var skillIconClone = (GameObject)Instantiate(firedSkillIconGO);
				skillIconClone.transform.SetParent(canvas.transform, false);
				//set sprite
				var img = skillIconClone.GetComponent<Image>();
				img.sprite = skillSprite.sprite;
				//set skill name
				//var text = skillIconClone.transform.Find("Text").GetComponent<Text>();
				//text.text = skillName;
				StartCoroutine(FadeOutSkill(skillIconClone));
				break;
			}
		}
	}

	private IEnumerator FadeOutSkill(GameObject skillIcon)
	{
		yield return new WaitForSeconds(0.7f);
		var img = skillIcon.GetComponent<Image>();
		for (float i = 1; i > 0; i -= 0.1f)
		{
			img.color = new Color(img.color.r, img.color.g, img.color.b, i);
			yield return new WaitForSeconds(0.05f);
		}
		GameObject.Destroy(skillIcon);
	}
}
