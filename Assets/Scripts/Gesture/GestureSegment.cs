using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GestureSegment
{
	public static double minLength = 100f;
	public List<Vector2> points = new List<Vector2>();

	public GestureSegment() {}

	public GestureSegment (List<Touch> touches_)
	{
		points = touches_.Select(x => x.position).ToList();
	}

	/*
	 * Scale down to origo, with a diagonal of its bounding box of 1.0.
	 */
	public static List<Vector2> scale(List<Vector2> gesture, float diagNeeded)
	{
		// calc centroid
		var centroid = new Vector2 (gesture.Average (x => x.x), gesture.Average (x => x.y));

		// push vectors to 0,0 centroid
		var scaled = new List<Vector2> (gesture);
		for (int i = 0; i < scaled.Count; ++i)
			scaled [i] -= centroid;
		
		// shring/enlarge bounding box to diagNeeded on both axes
		var diag = calcDiag(scaled);
		var boundingSizeReciprok = new Vector2(diagNeeded / diag, diagNeeded / diag);
		scaled.ForEach(x => x.Scale(boundingSizeReciprok));
		
		return scaled;
	}
	
	public static List<Vector2> scale(List<Touch> gesture, float diagNeeded)
	{
		return scale(gesture.Select(x => x.position).ToList(), diagNeeded);
	}
	
	public static float calcDiag(List<Vector2> gesture)
	{
		// find coordinates of the edges
		double lefty = float.MaxValue;
		double righty = float.MinValue;
		double upy = float.MinValue;
		double downy = float.MaxValue;
		foreach (var v in gesture)
		{
			lefty = (v.x < lefty) ? v.x : lefty;
			righty = (v.x > righty) ? v.x : righty;
			upy = (v.y > upy) ? v.y : upy;
			downy = (v.y < downy) ? v.y : downy;
		}
		
		// scale down to a bounding box of 1.0 width and height
		double width = (righty - lefty) > 0f ? (righty - lefty) : 1f;
		double height = (upy - downy) > 0f ? (upy - downy) : 1f;
		return (float) Math.Sqrt(width * width + height * height);
	}

	/*
	 * If n > touches size than search for longest deltaPositions
	 * and insert points there.
	 * If n < touches size than search for shortest deltaPositions
	 * and insert points there.
	 */
	public List<Vector2> resample(int n)
	{
		// build original list
		List<Vector2> resampled = points;
		List<Vector2> resampledDeltas = calculateDeltaPos(points);

		int diff = n - resampled.Count;
		if (diff > 0)
		{
			while (diff > 0)
			{
				// TODO finomitani lehet maxTaken = 1-el, de erősen lassúl
				int maxTaken = diff > (resampled.Count / 2) ? (resampled.Count / 2) : diff;
				diff -= maxTaken;
				var maxDeltaIndices = resampledDeltas.Select((value, index) => new {Value = value, Index = index})
					.OrderByDescending(x => x.Value.magnitude).Take(maxTaken).Select(x => x.Index);
				foreach (var i in maxDeltaIndices)
				{
					if (i - 1 >= 0) // not first element
						resampled.Insert(i, Vector2.Lerp(resampled[i - 1], resampled[i], .5f));
					else //if (i + 1 < resampled.Count) // not last element
						resampled.Insert(i, Vector2.Lerp(resampled[i], resampled[i + 1], .5f));
						
					resampledDeltas[i].Scale(new Vector2(.5f, .5f)); // slice half
					resampledDeltas.Insert(i, resampledDeltas[i]); // insert another half
				}
			}
		}
		else if (diff < 0)
		{
			diff *= -1;
			while (diff > 0)
			{
				resampledDeltas[0] = new Vector2(float.MaxValue, float.MaxValue); // 0th member always 0, don't want that to interfere
				int maxTaken = diff > (resampled.Count / 2) ? (resampled.Count / 2) : diff;
				diff -= maxTaken;
				var minDeltaIndices = resampledDeltas.Select((value, index) => new {Value = value, Index = index})
					.OrderBy(x => x.Value.magnitude).Take(maxTaken).Select(x => x.Index);
				resampled = resampled.Select((value, index) => new {Value = value, Index = index})
						.Where(x => ! minDeltaIndices.Contains(x.Index)).Select(x => x.Value).ToList();

				// recalculate deltas
				resampledDeltas = calculateDeltaPos(resampled);
				resampledDeltas.RemoveRange(resampled.Count, resampledDeltas.Count - resampled.Count);
			}
		}
		
		return resampled;
	}
	
	public static List<Vector2> calculateDeltaPos(List<Vector2> points)
	{
		var deltaPos = new List<Vector2>(points.Count);
		deltaPos.Add(new Vector2(0f, 0f)); // first delta
		
		for (int i = 1; i < points.Count(); ++i)
			deltaPos.Add(points[i] - points[i - 1]);
		
		return deltaPos;
	}
	
	public static List<GestureSegment> createTemplateSegments(List<GestureTouch> touches)
	{
		var segments = new List<GestureSegment> ();
		var seg = new GestureSegment ();
		float dist = 0f;
		for (int i = 0; i < touches.Count; ++i) {
			if (dist > minLength) {
				segments.Add (seg);
				dist = 0f;
				var seg2 = new GestureSegment ();
				seg2.points.AddRange (seg.points);
				seg = seg2;
			}
			seg.points.Add(touches[i].position);
			dist += touches[i].deltaPosition.magnitude;
		}
		if (seg.points.Count > 0)
			segments.Add (seg);
		
		//Debug.Log("SEGMENTS COUNT: " + segments.Count + "; TOUCHES COUNT: " + touches.Count);
		
		return segments;
	}
}
