﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class StupidAI : Controller
{
	private GameObject enemyGO;
	private GameObject startPlatform;
	
	public override void Start()
	{
		base.Start();
		enemyGO = creature.World.GetOtherCreature(creature).gameObject;
		startPlatform = creature.CurrentPlatform.gameObject;
		StartCoroutine(Wait());
	}

	public override void GameOver()
	{
		StopAllCoroutines();
	}


	IEnumerator Wait()
	{
		yield return new WaitForSeconds(2);
		StopCoroutine(Wait());
		StartCoroutine(FindEnemy());
	}

	IEnumerator FindEnemy()
	{
		while (true)
		{
			while (creature.CurrentPlatform == null)
				yield return null;
			var cLeft = creature.CurrentPlatform.GetCreature(Platform.Side.Left);
			var cRight = creature.CurrentPlatform.GetCreature(Platform.Side.Right);
			if ((cLeft != creature && cLeft != null) || (cRight != creature && cRight != null))
			{
				StopCoroutine(FindEnemy());
				StartCoroutine(Attack());
				break;
			}
			else
			{
				Move(enemyGO.transform.position - gameObject.transform.position);
				yield return new WaitForSeconds(1);
			}
		}
	}

	IEnumerator Attack()
	{
		for (int i = 0; i < 2; i++)
		{
			this.InterruptSequence(creature.skills[0]);
			yield return new WaitForSeconds(1);
		}
		StopCoroutine(Attack());
		StartCoroutine(Retreat());
	}

	IEnumerator Retreat()
	{
		int i = 4;
		while(true)
		{
			while (creature.CurrentPlatform == null)
				yield return null;
			Move(startPlatform.transform.position - gameObject.transform.position);
			i--;
			if (i == 0)
			{
				StopCoroutine(Retreat());
				StartCoroutine(Wait());
				break;
			}
			else
				yield return new WaitForSeconds(1);
		}
	}
}
