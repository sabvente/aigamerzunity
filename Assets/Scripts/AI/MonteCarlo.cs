﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// S. Ontanon, “Experiments with Game Tree Search in Real-Time Strategy Games,” arXiv:1208.1940 [cs], Aug. 2012.
/// online: http://arxiv.org/pdf/1208.1940.pdf

public struct BulletState
{
    public int pid;
    public float collisionTime;
    public Platform towards; // TODO exact trajectory may needed

    public BulletState(int pid_, float collisionTime_, Platform towards_)
    {
        pid = pid_;
        collisionTime = collisionTime_;
        towards = towards_;
    }

    public bool proceedTime(float dt)
    {
        collisionTime -= dt;
        return collisionTime <= 0;
    }
};

public struct PlayerState // FIXME TO CLASS
{
	public List<float> cooldowns;
	public Platform pos;
	public int hp;
	public float occupiedTill;
    public Queue<BulletState> bulletsTowardsPlayer;
	
	public PlayerState(Creature player)
	{
		pos = player.CurrentPlatform;
		hp = player.Health;
        if (player.gameObject.GetComponent<Controller>().occupiedBy != null)
            occupiedTill = player.gameObject.GetComponent<Controller>().occupiedBy.getCurrentOccupationTime();
        occupiedTill = 0;

        bulletsTowardsPlayer = new Queue<BulletState>(5);
        cooldowns = new List<float>(player.skills.Count);
        for (int i = 0; i < player.skills.Count; ++i)
            cooldowns.Add(0);
	}

    public PlayerState(PlayerState player)
	{
        bulletsTowardsPlayer = player.bulletsTowardsPlayer; // TODO para?
		cooldowns = player.cooldowns; // TODO para?
		pos = player.pos;
		hp = player.hp;
		occupiedTill = player.occupiedTill;
	}
	
	public void updateByRealGame(Creature player)
	{
		pos = player.CurrentPlatform;
		hp = player.Health;
		occupiedTill = player.gameObject.GetComponent<Controller>().occupiedBy.getCurrentOccupationTime();
		for (int i = 0; i < player.skills.Count; ++i)
		{
			if (player.skills[i].CanFire())
				cooldowns[i] = 0;
			else
				cooldowns[i] = player.skills[i].getCurrentCooldown();
		}
	}
	
	public List<int> actions()
	{
		if (occupiedTill > 0)
			return new List<int>();
		
		List<int> retval = new List<int>(cooldowns.Count);
		for (int i = 0; i < cooldowns.Count; ++i)
			if (cooldowns[i] <= 0) retval.Add(i);
		
        return retval;
	}
};

public struct GameState // FIXME TO CLASS
{
	public enum GameWinner {ongoing, draw, max, min}; // contr=max, enemy=min
	public GameWinner winner;
	public float t;
	public List<PlayerState> players;
    public int iteration;
	
	public GameState(List<Creature> creatures)
	{
		t = 0;
        iteration = 0;
		winner = 0;
		players = new List<PlayerState>(2);
		foreach (var cr in creatures)
			players.Add(new PlayerState(cr));

		updateByRealGame(creatures);
	}
	public GameState(GameState state)
	{
		t = state.t;
        iteration = 0;
		winner = state.winner;
		players = state.players;
	}
	
	public void updateByRealGame(List<Creature> creatures)
	{
		t = 0;
        iteration = 0;
		for (int i = 0; i < creatures.Count; ++i)
			players[i].updateByRealGame(creatures[i]);
        updateWinner();
	}
	
	// returns skill indices
	public List<int> playerActions(int pid)
	{
		return players[pid].actions();
	}
	
	public void proceedTime(float dt)
	{
		t += dt;
		for (int i = 0; i < players.Count; ++i)
		{
			PlayerState tmp = players[i];
            tmp.occupiedTill -= dt;
            for (int c = 0; c < tmp.cooldowns.Count; ++c) tmp.cooldowns[c] -= dt;
            // take out bullets and take damage if necessary
            var bulletDestroyed = true;
            while (bulletDestroyed && tmp.bulletsTowardsPlayer.Count > 0)
            {
                if (bulletDestroyed = tmp.bulletsTowardsPlayer.Peek().proceedTime(dt))
                {
                    var towards = tmp.bulletsTowardsPlayer.Dequeue().towards;
                    if (towards == tmp.pos) tmp.hp -= SphereBullet.damage; // take damage
                }
            }
			players[i] = tmp;
		}
	}

    public void updateWinner()
    {
        if (winner == GameWinner.ongoing)
        {
            var owndead = false;
            var enemydead = false;
            for (int i = 0; i < players.Count; ++i)
            {
                if (players[i].hp <= 0)
                {
                    if (i == 0) owndead = true;
                    else enemydead = true;
                }
            }
            if (owndead == true && enemydead == true) winner = GameState.GameWinner.draw;
            else if (owndead) winner = GameState.GameWinner.min;
            else if (enemydead) winner = GameState.GameWinner.max;
            //else winner = GameState.GameWinner.ongoing;
        }
    }

    static public int otherPlayerId(int pid)
    {
        return (pid == 0) ? 1 : 0;
    }
};

public class MonteCarlo
{
    public struct BestAction
    {
        public float val;
        public Skill action;

        public BestAction(float val_, Skill action_)
        {
            val = val_;
            action = action_;
        }

        public static BestAction max(BestAction a, BestAction b)
        {
            if (a.val < b.val) return b;
            return a;
        }

        public static BestAction min(BestAction a, BestAction b)
        {
            if (a.val > b.val) return b;
            return a;
        }
    };

    public static int maxIter = 5;
    public static float tmax = 1; // sec
	static float max(float a, float b) { return (a > b) ? a : b; }
	static float min(float a, float b) { return (a < b) ? a : b; }
	
	private float intel; // normalized
	private List<Creature> creatures; // creatures[0] = controlled

    public MonteCarlo(float intel_, List<Creature> creatures_) // creatures_[0] = controlled
	{
		intel = intel_;
		creatures = creatures_;
	}
	
	public Skill takeOverControl() // starts RTMM, step with contr
	{
		GameState gamestate = new GameState(creatures);
        var action = RTMM(gamestate, float.NegativeInfinity, float.PositiveInfinity);
        Debug.Log(action.action);
        Debug.Log(action.val);
        return action.action;
	}

    public BestAction RTMM(GameState state, float alpha, float beta) // TODO lépésSOROZATOT ADJON VISSZA?
	{
        ++state.iteration;
		if (state.iteration > maxIter || state.t >= tmax * intel || state.winner != GameState.GameWinner.ongoing)
			return new BestAction(evaluate(ref state), null);
		
		var actions = state.players[0].actions();
		if (actions.Count != 0) // controlled player
		{
            Debug.Log("controlled player act iteration: " + state.iteration);
			var b = new BestAction(float.NegativeInfinity, null);
			foreach (var a in actions)
			{
				var newstate = issueAction(state, 0, a);
                var ba = RTMM(state, alpha, beta);
                ba.action = creatures[0].skills[a];
                b = BestAction.max(b, ba);
                alpha = b.val;
                if (beta <= alpha) break; // pruning
			}
			return b;
		}
		int pid = 1;
		foreach (var player in state.players.GetRange(1, state.players.Count - 1)) // other players (opponents)
		{
			actions = player.actions();
			if (actions.Count != 0)
			{
                Debug.Log("enemy player act iteration: " + state.iteration);
				var b = new BestAction(float.PositiveInfinity, null);
				foreach (var a in actions)
				{
                    var newstate = issueAction(state, pid, a);
                    var ba = RTMM(state, alpha, beta);
                    ba.action = creatures[pid].skills[a];
                    b = BestAction.max(b, ba);
                    beta = b.val;
                    if (beta <= alpha) break; // pruning
				}
				return b;
			}
			++pid;
		}
        Debug.Log("simulate iteration: " + state.iteration);
		simulate(ref state);
		return RTMM(state, alpha, beta);
	}
	
	private void simulate(ref GameState state)
	{
		float minTimeToWait = 0.5f;
        foreach (var player in state.players)
            foreach (var cd in player.cooldowns)
                if (cd > 0.5) minTimeToWait = min(minTimeToWait, cd);
        
		state.proceedTime(minTimeToWait * 1.1f);
        state.updateWinner();
	}
	
	private GameState issueAction(GameState state, int pid, int action)
	{
		var playerstate = state.players[pid];
		playerstate.cooldowns[action] = creatures[pid].skills[action].cooldownTime;
		playerstate.occupiedTill = creatures[pid].skills[action].castTime;
		state.players[pid] = playerstate;
		creatures[pid].skills[action].applyToState (ref state, pid);
        state.updateWinner();
        return state;
	}
	
	private float evaluate(ref GameState state) // FIXME pass state by ref
	{
		return (state.players[0].hp - state.players.GetRange(1, state.players.Count - 1).Select(x => x.hp).Sum()
			/ (state.players.Count-1)) / state.t;
	}
}