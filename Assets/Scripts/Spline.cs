﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spline {

	public Vector2 startSpeed = new Vector2(0, 0);
	public Vector2 endSpeed = new Vector2(0, 0);
	public List<Vector2> controlPoints = new List<Vector2>();
	public List<Vector2> speedVectors = new List<Vector2>();
	public List<float> arrivalTimes = new List<float>();

	public void calculateSpeeds() {
		speedVectors.Clear();
		for (int i=0; i<controlPoints.Count; ++i) {
			if(i==0) {
				speedVectors.Add(startSpeed);
			}
			else if(i==controlPoints.Count-1) {
				speedVectors.Add(endSpeed);
			}
			else {
				Vector2 helper = ((controlPoints[i+1]-controlPoints[i])/(arrivalTimes[i+1]-arrivalTimes[i])
				                  + (controlPoints[i]-controlPoints[i-1])/(arrivalTimes[i]-arrivalTimes[i-1])) / 2;
				speedVectors.Add(helper);
			}
		}
	}

	public Vector2 calculatePosition(float t) {
		if (t < 0) {
			Debug.Log("Spline calculation with negative time!");
			return new Vector2();
		}
		if (arrivalTimes.Count <= 1) {
			Debug.Log("Spline calculation with zero or one point.");
			return new Vector2();
		}
		while (t > arrivalTimes[arrivalTimes.Count - 1]) {
			t = t - arrivalTimes[arrivalTimes.Count - 1];
		}

		int beginningSegmentPoint = 0;
		for (int i=1; i<arrivalTimes.Count; ++i) {
			if(arrivalTimes[i] > t) {
				beginningSegmentPoint = i-1;
				break;
			}
		}

		// Few extra variables for human readable code.
		// Compiler will optimize these out.
		float t1 = arrivalTimes[beginningSegmentPoint + 1];
		float t0 = arrivalTimes[beginningSegmentPoint];
		float dT = t - t0;
		float DT = t1 - t0;
		Vector2 x1 = controlPoints[beginningSegmentPoint + 1];
		Vector2 x0 = controlPoints[beginningSegmentPoint];
		Vector2 v1 = speedVectors[beginningSegmentPoint + 1];
		Vector2 v0 = speedVectors[beginningSegmentPoint];
		Vector2 a3 = (x0-x1)*(2/(DT*DT*DT)) + (v1+v0)/(DT*DT);
		Vector2 a2 = (x1-x0)*(3/(DT*DT)) - (v1+2*v0)/(DT);
		Vector2 a1 = v0;
		Vector2 a0 = x0;

		return a3*dT*dT*dT + a2*dT*dT + a1*dT + a0;
	}

	public static Spline createSpline(Vector2 from, Vector2 to, float height = 6f, float weight = 1f) {
		Spline spline = new Spline ();

		spline.controlPoints.Add(from);
		/*Vector2 jumpPeak;
		// Peak should be near the upper platform.
		if (from.y < to.y) {
			// Jumping up
			jumpPeak = (from + to + to + to) / 4;
			jumpPeak.y = to.y + height/weight;
			spline.startSpeed = (jumpPeak - from)/2;
			spline.endSpeed = (to - jumpPeak)/4;
		}
		else {
			// Jumping down
			jumpPeak = (from + from + from + to) / 4;
			jumpPeak.y = from.y + height/weight;
			spline.startSpeed = (jumpPeak - from) / 4;
			spline.endSpeed = (to - jumpPeak) / 2;
		}
		spline.controlPoints.Add(jumpPeak);*/
		spline.controlPoints.Add(to);

		spline.arrivalTimes.Add (0);
		//spline.arrivalTimes.Add (0.4f);
		spline.arrivalTimes.Add (0.5f);

		spline.calculateSpeeds();

		return spline;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
