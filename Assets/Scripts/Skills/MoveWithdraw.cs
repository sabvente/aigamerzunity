using System;
using UnityEngine;
using System.Linq;

public class MoveWithdraw : Skill
{
	private Creature enemy;
	protected override void Start()
	{
		base.Start();
		cooldownTime = 0.6f;
		castTime = 0.6f;
		Name = "Withdraw";
		Supertype = SkillSuperType.Move;
		Subtype = SkillSubType.Withdraw;
		enemy = Owner.World.GetOtherCreature(Owner);

        // DemoAI
        aggrLevel = 0f;
        period = 4;
	}

    public override float sense(DemoAI contr)
    {
        // more closer the better
        float multiplier;
        if (contr.creature.Team == Creature.Teams.Citadel) multiplier = 1;
        else multiplier = 4;
        return (float)Math.Exp(-(Vector2.Distance(contr.creature.transform.position, contr.otherAI.creature.transform.position) / 49f) * multiplier);
    }

	public override void overwrittenFire(Controller controller)
	{
        if (!controller.Move(controller.gameObject.transform.position - enemy.transform.position)) // orig
        {
            if (controller.creature.CurrentPlatform != null && enemy.CurrentPlatform != null
                && controller.creature.CurrentPlatform.gameObject == enemy.CurrentPlatform.gameObject) // same platform
                controller.Move(enemy.transform.position - controller.gameObject.transform.position);
            else // closest
            {
                var currdist = Vector2.Distance(enemy.transform.position, controller.creature.transform.position);
                var possiblePlatforms = GameObject.FindGameObjectsWithTag("Platform")
                    .Where(p =>
                    {
                        return Vector2.Distance(p.transform.position, enemy.transform.position) > currdist
                            && Vector2.Distance(p.transform.position, enemy.transform.position) - currdist > 3f;
                    }
                );

                if (possiblePlatforms == null) return;
                var possiblePlatformsList = possiblePlatforms.ToList();
                if (possiblePlatformsList.Count == 0) return;

                GameObject closestPlatform = null;
                var closestPlatformDist = float.MaxValue;
                foreach (var p in possiblePlatformsList)
                {
                    var d = Vector3.Distance(transform.position, p.transform.position);
                    if (closestPlatform == null || d < closestPlatformDist)
                    {
                        closestPlatform = p;
                        closestPlatformDist = d;
                    }
                }
                controller.Move(closestPlatform);
            }
        }
	}

    public override void applyToState(ref GameState state, int pid)
    {
        var opid = GameState.otherPlayerId(pid);
        var newplatform = Controller.PlatformInDir(state.players[pid].pos, state.players[pid].pos.transform.position - state.players[opid].pos.transform.position);
        if (newplatform == null) // orig
        {
            if (state.players[pid].pos == state.players[opid].pos)
                newplatform = Controller.PlatformInDir(state.players[pid].pos, state.players[opid].pos.transform.position - state.players[pid].pos.transform.position);
            if (newplatform == null)
            {
                var currdist = Vector2.Distance(state.players[opid].pos.transform.position, state.players[pid].pos.transform.position);
                var tmppos = state.players[opid].pos.transform.position;
                var possiblePlatforms = GameObject.FindGameObjectsWithTag("Platform")
                    .Where(p =>
                    {
                        return Vector2.Distance(p.transform.position, tmppos) > currdist
                            && Vector2.Distance(p.transform.position, tmppos) - currdist > 3f;
                    }
                );

                if (possiblePlatforms == null) return;
                var possiblePlatformsList = possiblePlatforms.ToList();
                if (possiblePlatformsList.Count == 0) return;

                GameObject closestPlatform = null;
                var closestPlatformDist = float.MaxValue;
                foreach (var p in possiblePlatformsList)
                {
                    var d = Vector3.Distance(state.players[pid].pos.transform.position, p.transform.position);
                    if (closestPlatform == null || d < closestPlatformDist)
                    {
                        closestPlatform = p;
                        closestPlatformDist = d;
                    }
                }
            }
            // apply
            if (newplatform != null)
            {
                var pstate = state.players[pid];
                pstate.pos = newplatform;
                state.players[pid] = pstate;
            }
        }
    }
}

