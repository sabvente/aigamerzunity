﻿using UnityEngine;
using System.Collections;

public class RushForesight : ForesightScenario
{
    public RushForesight()
    {
        // contrSeq & enemySeq depends on distance
        foresightMeter = 0.5f;
        period = 20f;
        textShowDur = 5f;
    }

    public override float applicability(DemoAI contr)
    {
        if (contr.creature.Team == Creature.Teams.Citadel && Scenario.stepDistance(contr) >= 3 && contr.foresight > foresightMeter)
            return (Time.time - appliedSince) / period * contr.foresight;
        return 0;
    }
    public override void apply(DemoAI contr)
    {
        if (applicability(contr) <= 0) return;
        base.apply(contr);

        // fill contrSeq & enemySeq
        var dist = Scenario.stepDistance(contr);
        for (int i = 0; i < dist; i += 2)
            contrSeq.Add(0);
        contrSeq.Add(3);
        contrSeq.Add(0);
        while (dist-- > 0)
            enemySeq.Add(2);
        enemySeq.Add(0);

        // fill seqs
        foreach (var s in contrSeq)
            contr.EnqueueSequence(contr.creature.skills[s]);
        foreach (var s in enemySeq)
            contr.otherAI.EnqueueSequence(contr.otherAI.creature.skills[s]);
        contr.world.AIText("Predicts enemy rush", contr.world.myCreature == contr.creature, textShowDur);
    }
}
