﻿using UnityEngine;
using System.Collections;

public class ParticleSystemSetLayer : MonoBehaviour {
	public string layer;
	void Start()
	{
		//Sorting layer cannot be set in the editor
		//http://answers.unity3d.com/questions/577288/particle-system-rendering-behind-sprites.html
		particleSystem.renderer.sortingLayerName = layer;
	}
}
