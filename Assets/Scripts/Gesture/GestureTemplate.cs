using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/*
 * Stores the segments of a gesture.
 * Calculates the probability of a given touch sequence being the stored gesture.
 */
public class GestureTemplate
{
	private static int occurrenceThreshold = 100;
	private static int allOccurrence = 0;
	private static double mixtureWeight = .5;
	private static double endPointWeight = .9;
	public static double varEuc = 80.0 * 80.0; // TODO dynamic calc
	public static double varAng = 3.14 * 3.14;
	
	public string name;
	public float diag;
	private List<GestureSegment> segments;
	private int nOccurred;
	
	public GestureTemplate (string name_, int nOccurred_, List<GestureSegment> segments_)
	{
		name = name_;
		nOccurred = nOccurred_;
		segments = segments_;
		allOccurrence += nOccurred_;
		diag = GestureSegment.calcDiag(segments_.Last().points); //TODO: if segments.Count == 0 Last() is broken
		
		// set centroid to 0
		var lastSegment = segments.Last().points;
		var centroid = new Vector2 (lastSegment.Average (x => x.x), lastSegment.Average (x => x.y));
		foreach (var seg in segments)
			for (int i = 0; i < seg.points.Count; ++i)
				seg.points[i] -= centroid;
	}
	
	public void occurred ()
	{
		++nOccurred;
	}
	
	// without adding the distribution / denumerator
	public double probability (List<Vector2> scaledGesture)
	{
		// calculate numerator
		double prior = (allOccurrence > occurrenceThreshold)
			? (double) nOccurred / (double) allOccurrence : 1.0 / (double) GestureRecognizer.templates.Count;
		double numerator = prior * likelihood(scaledGesture);
		
		return numerator;
	}
	
	private double getPriorProbability()
	{
		return (allOccurrence > occurrenceThreshold)
			? (double) nOccurred / (double) allOccurrence : 1.0 / (double) GestureRecognizer.templates.Count;
	}
	
	private double likelihood(List<Vector2> scaledGesture)
	{
		return segments.Select(seg => distance(scaledGesture, seg)).Max()
			* endPointDetection(scaledGesture);
	}
	
	public static double dotProduct(Vector2 a, Vector2 b)
	{
		return a.x * b.x + a.y * b.y;
	}
	
	private static double distance (List<Vector2> scaledGesture, GestureSegment template)
	{
		// calculate euclidean distance & angular difference
		double eucDist = .0;
		double angDiff = .0;
		var resampledTemplate = template.resample (scaledGesture.Count).ToList();
		var templateDelta = GestureSegment.calculateDeltaPos (resampledTemplate);
		var gestureDelta = GestureSegment.calculateDeltaPos (scaledGesture);
		
		// eaucledian distance
		for (int i = 0; i < scaledGesture.Count && i < resampledTemplate.Count; ++i)
			eucDist += (scaledGesture[i] - resampledTemplate[i]).magnitude;
		
		// angular distance
		for (int i = 1; i < templateDelta.Count && i < gestureDelta.Count; ++i)
			angDiff += Vector2.Angle(gestureDelta[i], templateDelta[i]) * 3.14f / 180f;
		
		eucDist /= (double)scaledGesture.Count;
		angDiff /= (double)scaledGesture.Count - 1;
		
		// FIXME standard normal distribution assumed - variable values between 0 and 1
		return Math.Exp(- (mixtureWeight * (eucDist*eucDist / varEuc) + (1.0 - mixtureWeight) * (angDiff*angDiff / varAng)));
	}
	
	private double endPointDetection (List<Vector2> scaledGesture)
	{
		return (1.0 + endPointWeight * Math.Exp(- Math.Pow(1.0 - distance(scaledGesture, segments.Last()), 2.0)));
	}
}