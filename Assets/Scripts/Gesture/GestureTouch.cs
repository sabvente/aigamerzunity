﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GestureTouch
{
	public Vector3 position;
	public Vector3 deltaPosition;

	public GestureTouch(Vector3 position, Vector3 deltaPosition)
	{
		this.position = position;
		this.deltaPosition = deltaPosition;
	}
}
