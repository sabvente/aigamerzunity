﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

/*
    list.Add("SphereBullet");   0
    list.Add("Defending");      1
    list.Add("MoveApproach");   2
    list.Add("MoveDodge");      3
    list.Add("MoveWithdraw");   4
    list.Add("Idle");           5
*/

public class Scenario
{
    protected float period = 15f; // 1 / frequency (sec)
    protected float textShowDur = 2f; // TODO set for scenarios

    public float appliedSince = 0;
    protected List<int> contrSeq = new List<int>();
    protected List<int> enemySeq = new List<int>();

    public virtual float applicability(DemoAI contr) { return 0; }
    public virtual void apply(DemoAI contr)
    {
        appliedSince = Time.time;
    }

    public static int stepDistance(DemoAI contr)
    {
        if (contr.creature.CurrentPlatform == null || contr.otherAI.creature.CurrentPlatform == null)
            return (int)Math.Ceiling((double)Vector2.Distance(contr.creature.transform.position, contr.otherAI.creature.transform.position) / 10.0);

        int stepcount = 0;
        Platform movingPlatform = contr.creature.CurrentPlatform;
        while (movingPlatform != contr.otherAI.creature.CurrentPlatform)
        {
            Vector2 to = (contr.otherAI.creature.transform.position - movingPlatform.transform.position).normalized;
            movingPlatform = Controller.PlatformInDir(movingPlatform, to);
            ++stepcount;
        }
        return stepcount;
    }

    public static float skillApplicable(DemoAI contr, int sid)
    {
        return contr.creature.skills[sid].getCurrentCooldown();
    }

    public static float seqApplicable(DemoAI contr, List<int> seq) // returns waiting time till applicability
    {
        float elapsed = 0;
        float waiting = 0;
        foreach (var s in seq)
        {
            var cd = contr.creature.skills[s].getCurrentCooldown();
            var cast = contr.creature.skills[s].castTime;
            if (cd > elapsed && waiting < cd - elapsed)
                waiting = cd - elapsed;
            elapsed += cast;
        }
        return waiting;
    }
}
