using System;
using UnityEngine;

public class MoveDodge : Skill
{
	private Creature enemy;
	protected override void Start()
	{
		base.Start();
		cooldownTime = 0.4f; // TODO calculate speed
		castTime = 0.4f;
		Name = "Dodge";
		Supertype = SkillSuperType.Move;
		Subtype = SkillSubType.Dodge;
		enemy = Owner.World.GetOtherCreature(Owner);

        // DemoAI
        aggrLevel = 0.5f;
        period = 5;
	}

    public override float sense(DemoAI contr)
    {
        return (float)Math.Exp( - (Vector2.Distance(contr.creature.transform.position, contr.otherAI.creature.transform.position) / 49f) * 30f);
    }

	public override void overwrittenFire(Controller controller)
	{
        var vec = enemy.transform.position - controller.creature.transform.position;
        if (!controller.Move(new Vector2(vec.y, -vec.x).normalized))
            controller.Move(new Vector2(-vec.y, vec.x).normalized);
	}

    public override void applyToState(ref GameState state, int pid)
    {
        //PlatformInDir
        var opid = GameState.otherPlayerId(pid);
        var vec = state.players[opid].pos.transform.position - state.players[pid].pos.transform.position;
        var newplatform = Controller.PlatformInDir(state.players[pid].pos, new Vector2(-vec.y, vec.x).normalized);
        if (newplatform == null)
            newplatform = Controller.PlatformInDir(state.players[pid].pos, new Vector2(vec.y, -vec.x).normalized);

        if (newplatform != null)
        {
            var pstate = state.players[pid];
            pstate.pos = newplatform;
            state.players[pid] = pstate;
        }
    }
}

