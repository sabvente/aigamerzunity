﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Controller : MonoBehaviour
{
	public static float moveAngleThreshold = 30f; // in degrees
    public static float maxJumpDistance = 15f;

	public Creature creature;
	public Queue<Skill> sequence = new Queue<Skill>();
	public Skill occupiedBy = null;

	public delegate void ActTriggered(SkillSuperType supertype, SkillSubType subtype, EventArgs e);
	public event ActTriggered Act;
	public event ActTriggered OwnAct;
	private float perceptionCounter = 0f;
	private Macro toFire = null;
	public World world;

	public virtual void Start()
	{
		creature = gameObject.GetComponent<Creature>();
		var worldGO = GameObject.Find("World");
		if (worldGO != null)
			world = worldGO.GetComponent<World>();
	}

	public virtual void GameOver() { }

	public virtual void Update()
	{
		perceptionCounter -= Time.deltaTime;
		if (perceptionCounter <= 0 && toFire != null)
		{
            perceptionCounter = 0;
			toFire.Fire(this);
			toFire = null;
		}
        if (toFire != null)
            toFire.Fire(this);
        
		if (sequence.Count > 0
			&& (occupiedBy == null || occupiedBy.finished) && this.creature.CurrentPlatform != null)
		{
			occupiedBy = sequence.Dequeue();
			if (occupiedBy.CanFire())
			{
				occupiedBy.Fire(this);
				ShowSkill(occupiedBy);
                //if (Act != null)
                //    Act(occupiedBy.Supertype, occupiedBy.Subtype, null); // TODO null eventargs
			}
		}
		else if (sequence.Count == 0
			&& (occupiedBy == null || occupiedBy.finished) && this.creature.CurrentPlatform != null)
		{
			// call idle event
			//if (OwnAct != null)
			//	OwnAct(SkillSuperType.Idle, SkillSubType.Default, null);
			sequence.Enqueue(GetComponent<Idle>());
		}
	}

	protected virtual void ShowSkill(Skill skill) { }

    static public Platform PlatformInDir(Platform from, Vector2 vector)
    {
        if (from == null) return null;

        // limit angle
        var possiblePlatforms = GameObject.FindGameObjectsWithTag("Platform")
            .Where(p =>
            {
                return Vector3.Angle(p.transform.position - from.transform.position, vector) <= moveAngleThreshold
                        && from.gameObject != p;
            }
            );
        if (possiblePlatforms == null) return null;

        var possiblePlatformsList = possiblePlatforms.ToList();
        if (possiblePlatformsList.Count == 0) return null; // TODO needed?

        // get the closest
        GameObject closestPlatform = null;
        var closestPlatformDist = float.MaxValue;
        foreach (var p in possiblePlatformsList)
        {
            var d = Vector3.Distance(from.transform.position, p.transform.position);
            if (closestPlatform == null || d < closestPlatformDist)
            {
                closestPlatform = p;
                closestPlatformDist = d;
            }
        }
        return closestPlatform.GetComponent<Platform>();
    }

	public bool Move(Vector2 vector)
	{
        var closestPlatform = PlatformInDir(creature.CurrentPlatform, vector);
        if (closestPlatform == null) return false;

        return Move(closestPlatform.gameObject);
	}

    public bool Move(GameObject toplatform)
    {
        if (creature.CurrentPlatform == null ||
            Vector2.Distance(creature.CurrentPlatform.transform.position, toplatform.transform.position) > maxJumpDistance) // too far
            return false;

        // approach / withdraw / dodge Acts
        var platformVec = toplatform.transform.position - creature.CurrentPlatform.transform.position;
        var playerVec = creature.World.GetOtherCreature(creature).transform.position - creature.transform.position;
        var angle = Vector2.Angle(platformVec, playerVec);
        if (angle <= 60f && Act != null)
            Act(SkillSuperType.Move, SkillSubType.Approach, null);
        else if (angle <= 120f && Act != null)
            Act(SkillSuperType.Move, SkillSubType.Dodge, null);
        else if (Act != null)
            Act(SkillSuperType.Move, SkillSubType.Withdraw, null);

        creature.JumpTo(toplatform.GetComponent<Platform>()); // FIXME add to sequence
        return true;
    }

	public void setToFire(Macro macro)
	{
		toFire = macro;
		perceptionCounter = creature.Perception;
	}

	public void InterruptSequence(Skill skill)
	{
		sequence.Clear();
		sequence.Enqueue(skill);
	}

	public void InterruptSequence(string skillName)
	{
		Skill skill = (Skill)gameObject.GetComponent(skillName);
		InterruptSequence(skill);
	}

	public void EnqueueSequence(Skill skill)
	{
		sequence.Enqueue(skill);
	}

	public void EnqueueSequence(string skillName)
	{
		Skill skill = creature.skills.Find(sk => sk.Name.Equals(skillName));
		if (skill != null)
			sequence.Enqueue(skill);
	}
}
