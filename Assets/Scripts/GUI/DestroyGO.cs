﻿using UnityEngine;
using System.Collections;

public class DestroyGO : MonoBehaviour {

	public void Destroy(GameObject go)
	{
		GameObject.Destroy(go);
	}
}
