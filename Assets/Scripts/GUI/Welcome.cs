﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Welcome : MonoBehaviour {

	public Sprite[] panelBackgrounds;
	public Sprite[] shadowsLeft;
	public Sprite[] shadowsRight;
	public GameObject background;
	public GameObject logo;
	public GameObject shadow1, shadow2;

	void Start () {
		shadow1.GetComponent<RawImage>().texture = shadowsLeft[Random.Range(0, shadowsLeft.Length - 1)].texture;
		shadow2.GetComponent<RawImage>().texture = shadowsRight[Random.Range(0, shadowsRight.Length - 1)].texture;
		StartCoroutine(ZoomBackground());
	}

	IEnumerator ZoomBackground()
	{
		var t = background.GetComponent<RectTransform>();
		t.localScale = new Vector3(
			t.localScale.x + 0.1f,
			t.localScale.y + 0.1f,
			t.localScale.z);
		while (t.localScale.x >= 1)
		{
			float a = 0.0001f;
			t.localScale = new Vector3(
				t.localScale.x - a,
				t.localScale.y - a,
				t.localScale.z);
			yield return new WaitForSeconds(0.003f);
		}
	}
}
