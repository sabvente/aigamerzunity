using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class WildRoar : Skill
{
	public int defense = 10;
	public int buffTime = 20;
	protected override void Start()
	{
		base.Start();
		cooldownTime = 30f;
		castTime = 0.2f;
		Name = "Wild roar";
		Supertype = SkillSuperType.Potion;
		Subtype = SkillSubType.Heal;
	}

	public override void overwrittenFire(Controller controller)
	{
		if (CanFire() && Owner.CurrentPlatform != null)
		{
			var w = Owner.gameObject.AddComponent<WildRoarBuff>();
			w.defense = defense;
			w.BuffTime = buffTime;
			w.Cast(Owner);
			base.Fire(controller);
		}
	}
}
