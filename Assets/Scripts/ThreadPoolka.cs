using System.Threading;
using System.Collections.Generic;
using UnityEngine;

public class ThreadPoolka
{
	public static int threadpoolSize = 3;
	private static long workTime = 0;
	private static readonly System.DateTime epoch = new System.DateTime(1970, 1, 1);
	
	private List<Thread> threads = new List<Thread>(threadpoolSize);
	List<long> timeStarted = new List<long>(threadpoolSize);
	private static readonly object syncLock = new object();
	
	public ThreadPoolka ()
	{
		for (int i = 0; i < threadpoolSize; ++i)
		{
			threads.Add(null);
			timeStarted.Add(0);
		}
	}
	
	public ThreadPoolka (int size)
	{
		threads.Capacity = size;
		for (int i = 0; i < size; ++i)
			threads.Add(null);
	}
	
	public static void updateWorkTime(long update)
	{
		lock (syncLock)
		{
			workTime += update;
			workTime /= 2;
		}
	}
	
	public void addWork(ThreadStart ts)
	{
		long currentTime = (long) System.DateTime.Now.Subtract(epoch).TotalMilliseconds;
		long freeInTime = long.MaxValue;
		int freeIndex = -1;
		
		// search for free threads
		for (int i = 0; i < threads.Count; ++i)
		{
			if (threads[i] == null || !threads[i].IsAlive)
			{
				freeIndex = i;
				break;
			}
			var elapsedSinceStart = currentTime - timeStarted[i];
			if (elapsedSinceStart < freeInTime)
			{
				freeInTime = elapsedSinceStart;
			}
		}
		
		// if one becomes free in << worktime
		if (freeIndex == -1 && freeInTime < workTime / 2)
			freeIndex = getFreeThreadIndex();
		
		if (freeIndex != -1)
		{
			threads[freeIndex] = new Thread(ts);
			threads[freeIndex].Start();
			timeStarted[freeIndex] = (long) System.DateTime.Now.Subtract(epoch).TotalMilliseconds;
		}
		else ts.Invoke();
	}
	
	public void waitAll()
	{
		foreach (var t in threads)
			if (t != null && t.IsAlive)
				t.Join();
	}
	
	private int tryGetFreeThreadIndex()
	{
		for (int i = 0; i < threads.Count; ++i)
			if (threads[i] == null || !threads[i].IsAlive)
				return i;
		
		return -1;
	}
	
	private int getFreeThreadIndex()
	{
		int freeIndex = -1;
		while (freeIndex == -1)
			freeIndex = tryGetFreeThreadIndex();
		return freeIndex;
	}
}
