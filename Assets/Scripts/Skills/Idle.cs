using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Idle : Skill
{	
	protected override void Start()
	{
		base.Start();
		cooldownTime = 0.2f;
		castTime = 0.2f;
		Name = "Idle";
		Supertype = SkillSuperType.Idle;
		Subtype = SkillSubType.Default;

        // DemoAI
        aggrLevel = 0.5f;
        period = 10;
    }

    public override float sense(DemoAI contr)
    {
        return UnityEngine.Random.Range(0f, 0.3f); // small value
	}
	
	public override void overwrittenFire(Controller controller)
	{
		//base.Fire(controller);
	}
}


