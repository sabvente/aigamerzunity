﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class Skill : MonoBehaviour
{
	public float cooldownTime { get; set; }
	public float lastCasted = 0;
	public float castTime { get; set; }
	private bool isOnCooldown;
	public string Name { get; set; }
	public SkillSuperType Supertype { get; set; }
	public SkillSubType Subtype { get; set; }
	public bool IsBasicAttack;
	public int Range { get; set; }
	public Creature Owner { get; set; }
	public bool finished = true;

    // DemoAI
    public float aggrLevel = 0; // [0,1]
    public float period = 4;
    public float appliedSince = float.NegativeInfinity + 1000;
    public virtual float sense(DemoAI contr) { return 0; }

	protected virtual void Start()
	{
		Owner = gameObject.GetComponent<Creature>();
	}

	public void Fire(Controller controller) {
		if (CanFire())
		{
			lastCasted = Time.time;
			finished = false;
			// Cast skill after the casting time has elapsed
			Owner.StartCoroutine(CastTimer(controller));
			// Start CD timer
			Owner.StartCoroutine(CooldownTimer());
		}
	}

	public virtual void overwrittenFire(Controller controller)
	{
		return;
	}

	public virtual float getCurrentCooldown() {
		if (Time.time - lastCasted < cooldownTime)
            return cooldownTime - (Time.time - lastCasted);
		else
			return 0;
	}

    public virtual float getCurrentOccupationTime()
    {
        if (Time.time - lastCasted < castTime)
            return castTime - (Time.time - lastCasted);
        else
            return 0;
    }

	public virtual void applyToState (ref GameState state, int pid) {}

	IEnumerator CastTimer(Controller controller)
	{
		yield return new WaitForSeconds(castTime);
        appliedSince = Time.time;
		overwrittenFire(controller);
		finished = true;
	}

	IEnumerator CooldownTimer()
	{
		isOnCooldown = true;
		yield return new WaitForSeconds(cooldownTime);
		isOnCooldown = false;
	}

	public virtual bool CanFire() {
		return !isOnCooldown;
	}
}
