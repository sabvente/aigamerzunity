using UnityEngine;
using System.Collections;
using System;

public class SphereBullet : Skill
{
    public static int damage = 60;
	public Platform towards = null;
	protected override void Start()
	{
		base.Start();
		cooldownTime = 1.3f;
		castTime = 0.8f;
		Name = "Sphere Bullet";
		Supertype = SkillSuperType.Attack;
		Subtype = SkillSubType.Ranged;
		IsBasicAttack = true;

        // DemoAI
        aggrLevel = 1;
        period = 4;
	}

    public override float sense(DemoAI contr)
    {
        // normal distribution
        double val = 0;
        var dist = Vector2.Distance(contr.creature.transform.position, contr.otherAI.creature.transform.position);
        val = 1.0 / (7.0 * Math.Sqrt(2.0 * Math.PI)) * Math.Exp(-(dist - 25.0) * (dist - 25.0) / (2.0 * 49.0)); // variance = 7, mean = 25f
        return (float) val * 40f;
    }
	
	public override void overwrittenFire(Controller controller)
	{
		Owner.isAttacking = true;
		Creature enemy = Owner.World.GetOtherCreature(Owner);
		//Levi: prefabba kéne tárolni, minden cuccot
		//Asszem most abban van, de a jelenlegi prefab position meg creature elhelyezeses rendszer nagyon nem rugalmas :D
		GameObject instance;
		if(Owner.name.Equals("Red")) {
			instance = (Instantiate(Resources.Load("Projectiles/redprojectile")) as GameObject);
		}
		else {
			instance = (Instantiate(Resources.Load("Projectiles/greenprojectile")) as GameObject);
		}
		instance.gameObject.transform.position = new Vector2(0, 0);
		Projectile p = instance.gameObject.GetComponent<Projectile>();
		p.enemy = enemy;
		p.owner = Owner;
			
		Spline spline = new Spline();
		Vector3 startPos;
		if(UnityEngine.Random.Range (0, 2) == 1)
			startPos = Owner.gameObject.transform.position;
		else
			startPos = Owner.gameObject.transform.position + new Vector3(0, 3.6f, 0);
		
		// TODO
		Vector3 endPos = new Vector3(0,0,0);
		if (towards == null)
			endPos = Owner.gameObject.transform.position + (enemy.gameObject.transform.position - startPos + new Vector3(0, 1.8f, 0)).normalized * 100;
		else
		{
            Debug.Log("sphere towards: " + towards);
			endPos = Owner.gameObject.transform.position + (towards.transform.position - startPos + new Vector3(0, 1.8f, 0)).normalized * 100;
			towards = null;
		}
		spline.controlPoints.Add(startPos);
		spline.controlPoints.Add(endPos);
		spline.startSpeed = new Vector3(0,0,0);
		spline.endSpeed = new Vector3(0,0,0);
		spline.arrivalTimes.Add(0);
		spline.arrivalTimes.Add(3f);
		spline.calculateSpeeds();
			
		p.trajectory = spline;
	}

    public override void applyToState(ref GameState state, int pid)
    {
        var opid = GameState.otherPlayerId(pid);
        float velocity = 100 / 3;
        float expectedCollisionTime = Vector2.Distance(state.players[pid].pos.transform.position, state.players[opid].pos.transform.position) / velocity;
        state.players[opid].bulletsTowardsPlayer.Enqueue(new BulletState(pid, expectedCollisionTime, state.players[opid].pos));
    }
}
