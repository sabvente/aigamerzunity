﻿using UnityEngine;
using System.Collections;

public class LearnDodge : LearnScenario
{
    public LearnDodge()
    {
        // fill learnContrSeq
        learnContrSeq.Add(5);
        learnContrSeq.Add(5);
        learnContrSeq.Add(5);
        learnContrSeq.Add(5);
        learnContrSeq.Add(2);
        // fill learnEnemySeq
        learnEnemySeq.Add(0);
        // fill contrSeq
        contrSeq.Add(3);
        contrSeq.Add(2);
        contrSeq.Add(2);
        // fill enemySeq
        enemySeq.Add(0);
        enemySeq.Add(5);
        enemySeq.Add(5);
        textShowDur = 4f;
    }

    public override float applicability(DemoAI contr)
    {
        var dist = Scenario.stepDistance(contr);
        if (contr.creature.Team == Creature.Teams.Gaia && dist >= 3 && dist < 5
            && seqApplicable(contr.otherAI, enemySeq) == 0f)
            return (Time.time - appliedSince) / period;
        return 0;
    }

    public override void apply(DemoAI contr)
    {
        if (applicability(contr) <= 0) return;
        base.apply(contr);
        
        var ctodo = contrSeq;
        var etodo = enemySeq;
        if (learnt > 0) // learn
        {
            ctodo = learnContrSeq;
            etodo = learnEnemySeq;
            contr.world.AIText("Learns to dodge", contr.world.myCreature == contr.creature, textShowDur);
		}
		else
		{
            contr.world.AIText("Dodges (learnt)", contr.world.myCreature == contr.creature, textShowDur);
		}

        foreach (var s in ctodo)
            contr.EnqueueSequence(contr.creature.skills[s]);
        foreach (var s in etodo)
            contr.otherAI.EnqueueSequence(contr.otherAI.creature.skills[s]);
    }
	
}
