﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Defense : Buff
{
	public int defense = 10;
	public Defense()
	{
		Name = "Defense";
		Supertype = SkillSuperType.Defense;
		Subtype = SkillSubType.Default;
		BuffTime = null;
	}
	public override int GetDamage(int damage) {
		return Math.Abs(damage - defense);
	}

}
