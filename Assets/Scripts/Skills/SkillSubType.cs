﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum SkillSubType
{
	Default,
	Physical,
	Withdraw,
	Dodge,
	Approach,
	Directed,
	Heal,
	Poison,
	Ranged
}
