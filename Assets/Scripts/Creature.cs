﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Creature : MonoBehaviour, Buffable
{
	//To be set in the editor
	public string CreatureName;
	public Teams Team;
	public int DefaultHealth;
	public float AttackRate;
	public float MovementSpeed;
	public float Perception;

	public World World;
	public int Health { get; protected set; }

	public List<Skill> skills = new List<Skill>();
	public List<Buff> buffs = new List<Buff>();
	public List<Skill> Sequence;
	public Platform lastPlatform;
	private Platform _currentPlatform;
	public Platform CurrentPlatform
	{
		get
		{
			return _currentPlatform;
		}
		set
		{
			lastPlatform = _currentPlatform;
			_currentPlatform = value;
		}
	}
	public Platform jumpToPlatform;
	public Spline jumpCurve = null;
	//todo - refactor later to use a newly created enum in place of Platform.Side (need a state called Air)
	public Platform.Side onSide;
	public bool isLeftHUD = false;
	public float jumpStartTime = 0;
	public Platform.Side jumpingToSide = Platform.Side.Undefined;

	public bool isJumping = false;
	public bool isAttacking = false;
	public bool isUsingSkill1 = false;
	public bool isUsingSkill2 = false;
	public bool isOnGround = true;
	public bool isDefending = false;

	// TODO audio hacks inside AudioManager or other
	private Dictionary<string, int> audioPlayed = new Dictionary<string, int>();
	private KeyValuePair<int, int> audioPlayDistribution = new KeyValuePair<int, int>();
	private KeyValuePair<AudioClip, float> currentlyPlaying = new KeyValuePair<AudioClip, float>(null, 0f);
	private static readonly System.Random getrandom = new System.Random();

	public enum Teams
	{
		Gaia,
		Citadel
	}

	void Awake()
	{
		var WorldGO = GameObject.Find("World");
		if (WorldGO != null)
			World = WorldGO.GetComponent<World>();
		audioPlayDistribution = new KeyValuePair<int, int>(1, 5); // TODO could be different for different skills / characters
		Health = DefaultHealth;
	}

	void Update()
	{
		//if creature is not in Game scene
		if (World == null)
			return;
		// Switch to the other side if needed. //TODO: Levi - WTF is this?
		var otherCreature = World.GetOtherCreature(this);
		if (otherCreature != null && ((otherCreature.gameObject.transform.position.x > gameObject.transform.position.x && gameObject.transform.localScale.x < 0)
			|| (otherCreature.gameObject.transform.position.x < gameObject.transform.position.x && gameObject.transform.localScale.x > 0)))
		{
			Vector3 temp = gameObject.transform.localScale;
			temp.x = -temp.x;
			gameObject.transform.localScale = temp;
		}

		if (isAttacking)
		{
			if (Random.Range(0, 6) == 0)
			{
				var s = AudioManager.getSound(CreatureName, "Attack");
				if(s != null)
					gameObject.GetComponent<AudioSource>().PlayOneShot(s);
			}
		}

		var animator = gameObject.GetComponent<Animator>();
		if (animator != null)
		{
			animator.SetBool("isJumping", isJumping);
			animator.SetBool("isAttacking", isAttacking);
			animator.SetBool("isUsingSkill1", isUsingSkill1);
			animator.SetBool("isUsingSkill2", isUsingSkill2);
			animator.SetBool("isDefending", isDefending);
		}
		isAttacking = false;
		isJumping = false;
		isUsingSkill1 = false;
		isUsingSkill2 = false;
		// Jumping things -- should be at the end of the function
		// Is in the air?
		if (onSide != Platform.Side.Undefined)
		{
			return;
		}
		else
		{
			isJumping = true;
		}
		// Is close to the destination?
		if (((gameObject.transform.position - jumpToPlatform.GetPosition(jumpingToSide)).magnitude < 1f))
		{
			gameObject.transform.position = jumpToPlatform.GetPosition(jumpingToSide);
			jumpToPlatform.StepOn(jumpingToSide, this);
			return;
		}
		// Should have arrived by now?
		if (Time.time - jumpStartTime > jumpCurve.arrivalTimes[jumpCurve.arrivalTimes.Count - 1])
		{
			gameObject.transform.position = jumpToPlatform.GetPosition(jumpingToSide);
			jumpToPlatform.StepOn(jumpingToSide, this);
			return;
		}
		gameObject.transform.position = jumpCurve.calculatePosition(Time.time - jumpStartTime);
	}

	private bool playSound(string clipName)
	{
		if (!audioPlayed.ContainsKey(clipName))
			audioPlayed.Add(clipName, audioPlayDistribution.Value);
		if (currentlyPlaying.Key != null
			&& Time.time - currentlyPlaying.Value > currentlyPlaying.Key.length)
			currentlyPlaying = new KeyValuePair<AudioClip, float>(null, 0f);

		++audioPlayed[clipName];
		if (currentlyPlaying.Key == null)
		{
			int rnd = getrandom.Next(audioPlayDistribution.Key, audioPlayDistribution.Value);
			if (audioPlayed[clipName] > rnd)
			{
				audioPlayed[clipName] = 0;
				var s = AudioManager.getSound(CreatureName, clipName);
				if(s != null)
					gameObject.GetComponent<AudioSource>().PlayOneShot(s);
				return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Makes the creature to jump to a platform.
	/// </summary>
	/// <returns>Returns false, if the jump is impossible.</returns>
	public bool JumpTo(Platform platform)
	{
		if (onSide == Platform.Side.Undefined)
		{
			return false;
		}
		if (platform.left == null && platform.jumpingToLeft == null)
		{
			jumpingToSide = Platform.Side.Left;
		}
		else if (platform.right == null && platform.jumpingToRight == null)
		{
			jumpingToSide = Platform.Side.Right;
		}
		else
		{
			return false;
			//cant jump -- cant happen with 2 creatures
		}

		jumpCurve = Spline.createSpline(CurrentPlatform.GetPosition(onSide), platform.GetPosition(jumpingToSide), 2);
		CurrentPlatform.Leave(this);
		jumpToPlatform = platform;
		platform.JumpTo(jumpingToSide, this);
		jumpStartTime = Time.time;
		isJumping = true;
		playSound("Jump");
		return true;
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		//if (col.gameObject.tag == "Platform")
		//{
		//	currentPlatform = col.gameObject.GetComponent<Platform>();
		//}
	}

	public void Heal(int hp)
	{
		Health += hp;
		if (Health > DefaultHealth)
			Health = DefaultHealth;
	}

	public void TakeDamage(int damage, Creature damager)
	{
		buffs.ForEach(ps => damage = ps.GetDamage(damage));
		if (isDefending)
			damage = 0;
		Health -= damage;

		var newSprite = (SpriteRenderer)gameObject.GetComponentInChildren<SpriteRenderer>();
		float ratio = 1 - (Health / (float)DefaultHealth);
		int centerX = newSprite.sprite.texture.width / 2;
		int centerY = newSprite.sprite.texture.height / 2;
		int r = (int)((centerX - 15) * ratio) + 15;
		Color transparent = new Color (1, 1, 1, 0);
		for (int i=centerX-r; i<centerX+r; ++i) {
			int ry = (int)Mathf.Sqrt(r*r-((centerX-i)*(centerX-i)));
			for (int j=centerY-ry; j<centerY+ry; ++j) {
				newSprite.sprite.texture.SetPixel (i, j, transparent);
			}
		}
		newSprite.sprite.texture.Apply ();

		/*Debug.Log (newSprite.sprite.texture.GetPixel (5, 5));
		for (int j=0; j<100; ++j)
			for (int i=0; i<100; ++i)
				newSprite.sprite.texture.SetPixel (5+j, 5 + i, new Color (1,1,1,0));*/


		if (Health < 0)
		{
			Died(damager);
		}
	}

	public void Died(Creature killer)
	{
		Health = 0;
		World.Killed(killer, this);
		PlayDying();
	}

	void PlayDying()
	{
		//TODO:PlayDying
		Destroy(this.gameObject);
	}

	public void AddBuff(Buff buff)
	{
		buffs.Add(buff);
	}

	public void RemoveBuff(Buff buff)
	{
		buffs.Remove(buff);
	}
}
