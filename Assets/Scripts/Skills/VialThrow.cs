using UnityEngine;
using System.Collections;

public class VialThrow : Skill
{
	protected override void Start()
	{
		base.Start();
		cooldownTime = 0.3f;
		castTime = 0.3f;
		Name = "Vial throw";
		Supertype = SkillSuperType.Attack;
		Subtype = SkillSubType.Ranged;
		IsBasicAttack = true;
	}

	public override void overwrittenFire(Controller controller)
	{
		if (CanFire() && Owner.CurrentPlatform != null)
		{
			Owner.isUsingSkill1 = true;
			Creature enemy = Owner.World.GetOtherCreature(Owner);
			//Levi: prefabba kéne tárolni, minden cuccot
			GameObject instance = (Instantiate(Resources.Load("Projectiles/vial")) as GameObject);
			instance.gameObject.transform.position = new Vector2(0, 0);
			Projectile p = instance.gameObject.GetComponent<Projectile>();
			instance.transform.localScale = new Vector3(1.4f, 1.4f);
			p.enemy = enemy;
			p.owner = Owner;

			Spline spline = new Spline();
			Vector3 startPos = Owner.gameObject.transform.position;
			Vector3 endPos = enemy.gameObject.transform.position;
			startPos.y += 5.4f;
			endPos.y += 4f;
			Vector3 throwPeak = (startPos + endPos) * 0.5f;
			throwPeak.y += 5.0f;
			startPos += throwPeak / throwPeak.magnitude;
			spline.controlPoints.Add(startPos);
			spline.controlPoints.Add(throwPeak);
			spline.controlPoints.Add(endPos);
			spline.startSpeed = (throwPeak - startPos);
			spline.endSpeed = endPos - throwPeak;
			spline.arrivalTimes.Add(0);
			spline.arrivalTimes.Add(0.25f);
			spline.arrivalTimes.Add(0.5f);
			spline.calculateSpeeds();

			p.trajectory = spline;

			base.Fire(controller);
		}
	}
}
