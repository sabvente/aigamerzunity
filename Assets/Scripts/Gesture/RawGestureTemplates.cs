﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class RawGestureTemplates : MonoBehaviour
{
	public List<GestureTemplate> templates = new List<GestureTemplate>();
	void Awake()
	{
		if (FindObjectsOfType(this.GetType()).Length > 1)
		{
			DestroyImmediate(gameObject);
			return;
		}
		DontDestroyOnLoad(this);
	}
}
