using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

/*
 * Tracks movement of one finger.
 */
public class GestureRecognizer : MonoBehaviour
{
	private static double threshold = .40; // above this certainty action is evoked when gesture ended
	//private static double autoEndThreshold = .6; // if all certainties substracted from the maximum exceeds this, movement can be evoked before end of gesture 
	private static int enoughTouchToAutoend = 4;
	private static double varianceThreshold = .4; // when recognizing direction of swipes; the variance of coordinates below a swipe will be recognized
	private static int countThreshold = 10; // number of touches needed for a swipe to be recognized
	private static int windowSize = 4;
	private static double trueBiasedPreventor = .15; // added to the bayes denominator: denum = (1 + (this / num of gestures)) * denum
	public static List<GestureTemplate> templates = new List<GestureTemplate>();

	public int fingerID = 0;
	private List<Touch> touches = new List<Touch>(); // so far
	private List<Vector2> mTouches = new List<Vector2>();
	private List<Queue<double>> probWindows = new List<Queue<double>>(); // to be able to avg over window
	private int bestFit; // so far - index templates
	private bool finished = false;
	public Controller controller = null; // set in PlayerController

	private ThreadPoolka threadPool = new ThreadPoolka();
	private readonly object syncLock = new object();

	// Use this for initialization
	void Start()
	{
		clear();
		var rgt = GameObject.Find("RawGestureTemplates");
		if (rgt != null)
			rgt.GetComponent<RawGestureTemplates>().templates.ForEach(t => addTemplate(t));

		string deb = "";
		foreach (var tt in templates)
			deb += tt.name + ", ";
		Debug.Log("Templates: " + deb);
		/*else
		{
			for (int t = 0; t < templates.Count; ++t)
			{
				var dummyQueue = new Queue<double>();
				for (int i = 0; i < windowSize; ++i)
					dummyQueue.Enqueue(-1.0);
				probWindows.Add(dummyQueue);
			}
		}*/
	}

	// Update is called once per frame
	void Update()
	{
		// mouse
		/*var mTouch = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		if (mTouch.x > Screen.width / 2)
		{
			if (Input.GetMouseButtonDown(0))
			{
				clear();
				finished = false;
				mTouches.Add(mTouch);
			}

			else if (Input.GetMouseButton(0))
			{
				if (finished) return;
				mTouches.Add(mTouch);
				threadPool.addWork(new ThreadStart(refreshProbabilities));
			}
			else if (Input.GetMouseButtonUp(0))
			{
				if (touches.Count == 1) // only begin
					controller.InterruptSequence(creature.skills[0]);
				// more than tap -> skill
				else
				{
					mTouches.Add(mTouch);
					//refreshProbabilities();
					threadPool.waitAll();

					// TODO debug
					Debug.Log("COUNT: " + mTouches.Count);
					string dstr = "PROBWINDOW: ";
					foreach (var w in probWindows)
					{
						dstr += "WINDOW: ";
						foreach (var p in w)
							dstr += p + ", ";
						dstr += ";  ";
					}
					Debug.Log(dstr);

					var templatesProb = probWindows.Select(t => calcWindowAvg(t)).ToList();
					double max = templatesProb.Max();
					if (2.0 * max - templatesProb.Sum() > threshold)
					{
						var temp = templates[templatesProb.IndexOf(max)];
						temp.occurred();
						controller.InterruptSequence(creature.skills[0]);
					}
				}
			}
		}*/

		// find touch with the given fingerID
		Touch touch = new Touch();
		bool found = false;
		int i = 0;
		while (!found && i < Input.touchCount)
		{
			if (Input.GetTouch(i).fingerId == fingerID)
			{
				touch = Input.GetTouch(i);
				found = true;
				break;
			}
			++i;
		}
		if (!found) return;

		//SKILL MODE
		if (controller.world.IsSkillMode)
		{
			// check touch phase
			if (touch.phase == TouchPhase.Began)
			{
				clear();
				finished = false;
				touches.Add(touch);
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				if (finished) return;
				touches.Add(touch);
				threadPool.addWork(new ThreadStart(refreshProbabilities));

				// if not enough touch, don't judge
				if (touches.Count < enoughTouchToAutoend)
					return;

				// auto end TODO turned out because segment size ~ 1 (huge minLength)
				/*var templatesProb = probWindows.Select(t => calcWindowAvg(t)).ToList();
				double max = templatesProb.Max();

				// TODO debug
				//string probDebug = "";
				//foreach (var p in templatesProb)
					//probDebug += p + ", ";
				//Debug.Log ("MOVED-AUTOEND templateProb: " + probDebug);
				//Debug.Log ("MOVED-AUTOEND: max: " + max + ", probsum: " + templatesProb.Sum() + ", max kiemelkedese: " + (2.0 * max - templatesProb.Sum ()));

				if (2.0 * max - templatesProb.Sum () > autoEndThreshold) {
					//Debug.Log ("ENNYIBOL ATMENT: " + (2.0 * max - templatesProb.Sum ()));
					var temp = templates [templatesProb.IndexOf (max)];
					temp.occurred ();
					gameObject.GetComponent<PlayerController> ().FireSkill ("!!!!! PREPRE " + temp.name);
					finished = true;
					clear();
				}*/
			}
			else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
			{
				if (finished) return;

				// more than tap -> skill
				if (touches.Count != 1)
				{
					touches.Add(touch);
					//refreshProbabilities();
					threadPool.waitAll();

					var templatesProb = probWindows.Select(t => calcWindowAvg(t)).ToList();
					double max = templatesProb.Max();

					// TODO debug
					string dstr = "PROBWINDOW: ";
					foreach (var w in probWindows)
					{
						dstr += "WINDOW: ";
						foreach (var p in w)
							dstr += p + ", ";
						dstr += ";  ";
					}
					Debug.Log(dstr);

					// TODO debug
					//string probDebug = "";
					//foreach (var p in templatesProb)
					//	probDebug += p + ", ";
					//Debug.Log ("ENDED templateProb: " + probDebug);
					//Debug.Log ("ENDED: max: " + max);
					if (2.0 * max - templatesProb.Sum() > threshold)
					{
						var temp = templates[templatesProb.IndexOf(max)];
						controller.InterruptSequence(temp.name);
						temp.occurred();
					}
					controller.creature.World.SwitchMode();
				}
				clear();
			}
		}
		// MOVE AND BASIC ATTACK MODE
		else
		{
			if (touch.phase == TouchPhase.Began)
			{
				clear();
				finished = false;
				touches.Add(touch);
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				touches.Add(touch);
				var variance = calcTouchVariance();
				if (touches.Count > countThreshold
					&& variance.x < varianceThreshold
					&& variance.y < varianceThreshold)
				{
					finished = true;
					controller.Move(calcTouchDirection());
				}
			}
			else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
			{
				// tap -> basic attack
				if (touches.Count == 1) // only begin
					controller.InterruptSequence(controller.creature.skills[0]);
				else
				{
					var variance = calcTouchVariance();
					if (variance.x < varianceThreshold
						&& variance.y < varianceThreshold)
					{
						finished = true;
						controller.Move(calcTouchDirection());
					}
				}
			}

		}
	}

	private void refreshProbabilities()
	{
		var watch = System.Diagnostics.Stopwatch.StartNew();

		// TODO same diag for all templates
		List<List<Vector2>> scaledGestures = null;
		lock (syncLock)
		{
			if (mTouches.Count > 0)
				scaledGestures = templates.Select(seg => GestureSegment.scale(mTouches, seg.diag)).ToList();
			else
				scaledGestures = templates.Select(seg => GestureSegment.scale(touches, seg.diag)).ToList();
		}
		var tmpProb = new List<double>(templates.Count);

		// calculate denumerator & save probs
		double denumerator = 0.0;
		for (int t = 0; t < templates.Count; ++t)
		{
			// todo multithread
			tmpProb.Add(templates[t].probability(scaledGestures[t]));
			denumerator += tmpProb[t];
		}
		denumerator += denumerator * trueBiasedPreventor / templates.Count;

		// calculate probabilities
		for (int t = 0; t < templates.Count; ++t)
			addProb(t, tmpProb[t] / denumerator);

		watch.Stop();
		ThreadPoolka.updateWorkTime(watch.ElapsedMilliseconds);
	}

	private static double calcWindowAvg(Queue<double> window)
	{
		double res = 0.0;
		int num = 0;
		foreach (var w in window)
		{
			if (w >= 0)
			{
				res += w;
				num += 1;
			}
		}

		num = (num == 0) ? 1 : num;
		return res / (double)num;
	}

	public Vector2 calcTouchVariance()
	{
		var avgX = touches.Select(t => t.deltaPosition.normalized.x).Average();
		var avgY = touches.Select(t => t.deltaPosition.normalized.y).Average();
		var varX = touches.Select(t => Math.Pow(t.deltaPosition.normalized.x - avgX, 2.0)).Sum() / touches.Count;
		var varY = touches.Select(t => Math.Pow(t.deltaPosition.normalized.y - avgY, 2.0)).Sum() / touches.Count;

		return new Vector2((float)varX, (float)varY);
	}

	public Vector2 calcTouchDirection() // FIXME lefelére nézd meg
	{
		return new Vector2(touches.Select(t => t.deltaPosition.x).Sum(),
					   touches.Select(t => t.deltaPosition.y).Sum()).normalized;
	}

	public void addTemplate(GestureTemplate template)
	{
		templates.Add(template);
		var dummyQueue = new Queue<double>();
		for (int i = 0; i < windowSize; ++i)
			dummyQueue.Enqueue(-1.0);
		probWindows.Add(dummyQueue);
	}

	public string getBestFitGesture()
	{
		if (bestFit != -1)
			return templates[bestFit].name;
		return null;
	}

	public void clear()
	{
		bestFit = -1;
		lock (syncLock)
		{
			touches.Clear();
			mTouches.Clear();
		}

		// probWindows clear
		foreach (var w in probWindows)
		{
			w.Clear();
			for (int i = 0; i < windowSize; ++i)
				w.Enqueue(-1.0);
		}
	}

	private void addProb(int templateIndex, double prob)
	{
		lock (syncLock)
		{
			probWindows[templateIndex].Dequeue();
		}
		probWindows[templateIndex].Enqueue(prob);
	}
}

