﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TeachGesture : MonoBehaviour
{
	//To be set in editor
	public GameObject skillScrollPanelGO;
	public GameObject skillIconGO;
	public GameObject creatureFactoryGO;
	public GameObject boundsPanelGO;
	public GameObject particleGO;
	public GameObject helpGO;
	public GameObject skillStoreGO;
	public GameObject lineGO;

	private RawGestureTemplates rawGestureTemplates;
	private Dictionary<string, List<GestureTouch>> learnedGestures = new Dictionary<string, List<GestureTouch>>();
	private ParticleSystem currentParticleSystem;
	private string currentSkillName;
	private Vector3 prevMousePosition;
	private Bounds gestureBounds;
	private bool isDrawing;
	private LineRenderer lineRenderer;
	private TeachGestureController controller;

	void Start()
	{
		//TOOD: remove temp var
		var creatureName = "Igor";
		var cs = GameObject.Find("SelectedCreature");
		if (cs != null)
			creatureName = cs.GetComponent<SelectedCreature>().selected;

		rawGestureTemplates = GameObject.Find("RawGestureTemplates").GetComponent<RawGestureTemplates>();

		var creatureFactory = creatureFactoryGO.GetComponent<CreatureFactory>();
		var skillNames = creatureFactory.GetGestureSkills(creatureName);
		int addedSkills = 0;
		var ssGO = (GameObject)Instantiate(skillStoreGO);
		SkillStore skillStore = ssGO.GetComponent<SkillStore>();
		foreach (var skillSprite in skillStore.skills)
		{
			if (skillNames.Contains(skillSprite.skillName))
			{
				string skillName = skillSprite.skillName;
				var skillIconClone = (GameObject)Instantiate(skillIconGO);
				skillIconClone.transform.parent = skillScrollPanelGO.transform;
				var rect = skillIconClone.GetComponent<RectTransform>();
				rect.anchoredPosition = new Vector2(0, -addedSkills * (rect.sizeDelta.y + 20) - 10);
				//set sprite
				var img = skillIconClone.transform.Find("Image").GetComponent<RawImage>();
				img.texture = skillSprite.sprite.texture;
				//set skill name
				var text = skillIconClone.transform.Find("Text").GetComponent<Text>();
				text.text = skillName;
				//add click handler
				var btn = skillIconClone.GetComponent<Button>();
				btn.onClick.AddListener(() =>
				{
					SelectSkill(skillIconClone, skillName);
				});

				learnedGestures.Add(skillName, new List<GestureTouch>());

				addedSkills++;
			}
		}

		//set up recording
		Input.simulateMouseWithTouches = false;
		prevMousePosition = Input.mousePosition;

		//get bounds of gesture
		Vector3[] vecs = new Vector3[4];
		boundsPanelGO.GetComponent<RectTransform>().GetWorldCorners(vecs);
		foreach (var v in vecs)
		{
			gestureBounds.Encapsulate(v);
		}
		gestureBounds.Expand(new Vector3(0, 0, 100));

		//create creature
		var p = GameObject.Find("CreaturePos").transform;
		var c = creatureFactory.SpawnCreature(creatureName, p.transform.position);
		c.gameObject.transform.localScale = new Vector3(
			-c.gameObject.transform.localScale.x*20,
			c.gameObject.transform.localScale.y*20,
			c.gameObject.transform.localScale.z*20);
		controller = c.gameObject.AddComponent<TeachGestureController>();

		//show help
		//var canvas = GameObject.Find("Canvas");
		//var help = (GameObject)GameObject.Instantiate(helpGO);
		//help.transform.parent = canvas.transform;
	}

	public void SelectSkill(GameObject skillIcon, string skillName)
	{
		currentSkillName = skillName;
		var backs = GameObject.FindObjectsOfType<RawImage>().Where(t => t.name == "SkillIconBack");
		foreach (var item in backs)
		{
			item.color = new Color(0, 0, 0, 0);
		}
		var bg = skillIcon.transform.Find("SkillIconBack").GetComponent<RawImage>();
		bg.color = Color.white;
	}

	void Update()
	{

		if (currentSkillName == null)
			return;
		bool posCorrected = false;
		Vector3 correctedPos;
		GestureTouch gestureTouch;
		if (Input.touches.Length > 0)
		{

			var touch = Input.touches[0];

			if (CorrectPos(Camera.main.ScreenToWorldPoint(touch.position), out correctedPos))
			{
				posCorrected = true;
				gestureTouch = new GestureTouch(Camera.main.WorldToScreenPoint(correctedPos), Vector3.zero);
			}
			else
				gestureTouch = new GestureTouch(touch.position, touch.deltaPosition);

			if (touch.phase == TouchPhase.Began)
			{
				if (!posCorrected)
				{
					if (currentParticleSystem != null)
						GameObject.Destroy(currentParticleSystem.gameObject);
					learnedGestures[currentSkillName] = new List<GestureTouch>();
					var pGO = (GameObject)Instantiate(particleGO, correctedPos + Vector3.forward * 10f, Quaternion.identity);
					currentParticleSystem = pGO.particleSystem;
					isDrawing = true;
				}
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				if (isDrawing)
				{
					learnedGestures[currentSkillName].Add(gestureTouch);
					currentParticleSystem.transform.position = correctedPos + Vector3.forward * 10f;
				}
			}
			else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
			{
				if (isDrawing)
				{
					isDrawing = false;
					learnedGestures[currentSkillName].Add(gestureTouch);
					currentParticleSystem.emissionRate = 0;
					GameObject.Destroy(currentParticleSystem.gameObject, 2);
					controller.InterruptSequence(currentSkillName);
				}
			}
		}
		posCorrected = false;

		if (CorrectPos(Camera.main.ScreenToWorldPoint(Input.mousePosition), out correctedPos))
		{
			posCorrected = true;
			gestureTouch = new GestureTouch(Camera.main.WorldToScreenPoint(correctedPos), Vector3.zero);
		}
		else
			gestureTouch = new GestureTouch(Input.mousePosition, prevMousePosition - Input.mousePosition);

		if (Input.GetMouseButtonDown(0))
		{
			if (!posCorrected)
			{
				if (currentParticleSystem != null)
					GameObject.Destroy(currentParticleSystem.gameObject);
				learnedGestures[currentSkillName] = new List<GestureTouch>();
				var pGO = (GameObject)Instantiate(particleGO, correctedPos + Vector3.forward * 10f, Quaternion.identity);
				currentParticleSystem = pGO.particleSystem;
				isDrawing = true;
			}
		}
		if (Input.GetMouseButton(0))
		{
			if (isDrawing)
			{
				learnedGestures[currentSkillName].Add(gestureTouch);
				currentParticleSystem.transform.position = correctedPos + Vector3.forward * 10f;
			}
		}
		if (Input.GetMouseButtonUp(0))
		{
			if (isDrawing)
			{
				isDrawing = false;
				learnedGestures[currentSkillName].Add(gestureTouch);
				currentParticleSystem.emissionRate = 0;
				GameObject.Destroy(currentParticleSystem.gameObject, 2);
				controller.InterruptSequence(currentSkillName);
			}
		}
		prevMousePosition = Input.mousePosition;
	}

	/// <summary>
	/// Corrects position to be in the bounds
	/// </summary>
	bool CorrectPos(Vector3 pos, out Vector3 newPos)
	{
		if (gestureBounds.Contains(pos))
		{
			newPos = pos;
			return false;
		}
		else
		{
			Vector3 dir = (gestureBounds.center - pos).normalized;
			Ray r = new Ray(gestureBounds.center, dir);
			float t;
			gestureBounds.IntersectRay(r, out t);
			newPos = gestureBounds.center + dir * t;
			return true;
		}
	}

	public void Save()
	{
		foreach (var item in learnedGestures)
		{
			rawGestureTemplates.templates.Add(
				new GestureTemplate(item.Key, 1, GestureSegment.createTemplateSegments(item.Value)));
		}
	}
}
