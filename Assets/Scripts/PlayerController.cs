﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlayerController : Controller
{
	private Dictionary<int, GestureRecognizer> gestureRecognizers;
	public GameObject gestureRecognizer;
	public GameObject gestureParticleSystem;
	public GameObject gestureWind;
	private Dictionary<int, GameObject> particleSystems = new Dictionary<int, GameObject>();
	private Dictionary<int, GameObject> gestureWinds = new Dictionary<int, GameObject>();

	// Use this for initialization
	public override void Start()
	{
		base.Start();
		gestureRecognizers = new Dictionary<int, GestureRecognizer>();
		for (int i = 0; i < 1; i++) // TODO migrate to World  | WTF??? - Levi
		{
			var gr = gameObject.AddComponent<GestureRecognizer>();
			gr.fingerID = i;
			gr.controller = this;
			gestureRecognizers.Add(i, gr);
		}
	}

	// Update is called once per frame
	public override void Update()
	{
		if (world.IsSkillMode)
		{
			foreach (var touch in Input.touches)
			{
				if (touch.phase == TouchPhase.Began)
				{
					var newGO = (GameObject)Instantiate(gestureParticleSystem, Camera.main.ScreenToWorldPoint(touch.position) + Vector3.forward * 10f, Quaternion.identity);
					if (particleSystems.ContainsKey(touch.fingerId))
					{
						particleSystems[touch.fingerId] = newGO;
					}
					else
					{
						particleSystems.Add(touch.fingerId, newGO);
					}
					newGO = (GameObject)Instantiate(gestureWind, Camera.main.ScreenToWorldPoint(touch.position) + Vector3.forward * 10f, Quaternion.identity);
					if (gestureWinds.ContainsKey(touch.fingerId))
					{
						gestureWinds[touch.fingerId] = newGO;
					}
					else
					{
						gestureWinds.Add(touch.fingerId, newGO);
					}
				}
				else if (touch.phase == TouchPhase.Moved)
				{
					particleSystems[touch.fingerId].particleSystem.transform.position = Camera.main.ScreenToWorldPoint(touch.position) + Vector3.forward * 10f;
					gestureWinds[touch.fingerId].transform.position = Camera.main.ScreenToWorldPoint(touch.position) + Vector3.forward * 10f;
				}
				else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
				{
					GameObject.Destroy(particleSystems[touch.fingerId]);
					GameObject.Destroy(gestureWinds[touch.fingerId]);
				}
			}
		}

		/*if (Input.GetButtonDown("Fire1"))
		{
			creature.World.enemyCreature.Died(null);
		}*/

		// moving on PC TODO just for testing
		var moveVector = new Vector2();
		if (Input.GetKey(KeyCode.RightArrow))
		{
			moveVector.x += 1f;
		}
		else if (Input.GetKey(KeyCode.LeftArrow))
		{
			moveVector.x += -1f;
		}
		if (Input.GetKey(KeyCode.UpArrow))
		{
			moveVector.y += 1f;
		}
		else if (Input.GetKey(KeyCode.DownArrow))
		{
			moveVector.y += -1f;
		}
		Move(moveVector);
		// basic skill test PC TODO just for testing
		if (Input.GetButtonDown("Fire2"))
		{
			//creature.isAttacking = true;
			//this.InterruptSequence(creature.skills.Find(s => s.Name == "Vial throw"));
		}


		// keydown for skills
		if (Input.GetKeyDown(KeyCode.Q) && creature.skills.Count > 0)
			InterruptSequence(creature.skills[0]);
		else if (Input.GetKeyDown(KeyCode.W) && creature.skills.Count > 1)
			InterruptSequence(creature.skills[1]);
		else if (Input.GetKeyDown(KeyCode.E) && creature.skills.Count > 2)
			InterruptSequence(creature.skills[2]);
		else if (Input.GetKeyDown(KeyCode.R) && creature.skills.Count > 3)
			InterruptSequence(creature.skills[3]);

		base.Update();
	}

	protected override void ShowSkill(Skill skill)
	{
		base.ShowSkill(skill);
		if (!skill.IsBasicAttack && creature.World.myCreature == creature)
			creature.World.FadeOutSkill(skill.GetType().ToString());
	}
}
