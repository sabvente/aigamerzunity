﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum SkillSuperType
{
	Potion,
	Move,
	Attack,
	Defense,
	Idle

}
