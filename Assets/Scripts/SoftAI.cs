using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SoftAI
{
	public Dictionary<SkillSuperType, Dictionary<SkillSubType, Macro>> macroTable
		= new Dictionary<SkillSuperType, Dictionary<SkillSubType, Macro>>();

	public Controller owner;

	public SoftAI(Controller owner_, Controller enemy)
	{
		owner = owner_;
		enemy.Act += React;
		owner.OwnAct += React;
	}

	private void React(SkillSuperType supertype, SkillSubType subtype, EventArgs e)
	{
		Debug.Log("REACT: " + supertype + " / " + subtype);
		// TODO here AI should wait according to perception - so it should be a monobehavior
		if (macroTable.ContainsKey(supertype))
		{
			Debug.Log("REACT SUPER FOUND");
			var allMacro = macroTable[supertype];

			Macro macro = null;
			if (allMacro.ContainsKey(subtype))
				macro = allMacro[subtype];
			else if (allMacro.ContainsKey(SkillSubType.Default))
				macro = allMacro[SkillSubType.Default];

			if (macro != null)
			{
				Debug.Log("REACT SUPER, SUB FOUND; FIRED " + macro.Name);
				owner.setToFire(macro);
				//macro.Fire(owner);
			}
		}
	}
}

