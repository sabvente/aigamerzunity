﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class ForesightScenario : Scenario
{
    protected float foresightMeter = 1; // default

    public override float applicability(DemoAI contr) { return 0; }
    public override void apply(DemoAI contr)
    {
        base.apply(contr);
    }

}
