﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager {
	private static string playerCreatureName;
	private static string opponentCreatureName;
	private static List<KeyValuePair<string, AudioClip>> playerAudio = new List<KeyValuePair<string, AudioClip>>();
	private static List<KeyValuePair<string, AudioClip>> opponentAudio = new List<KeyValuePair<string, AudioClip>>();
	private static AudioClip silence;

	public static void initialize(string playerName, string opponentName) {
		playerCreatureName = playerName;
		opponentCreatureName = opponentName;
		loadSounds(playerName, playerAudio);
		loadSounds(opponentName, opponentAudio);
	}

	private static void loadSounds(string creatureName, List<KeyValuePair<string, AudioClip>> clipList) {
		int numOfInitializedCharacterSounds = 0;
		// Load empty sound asset for unknown creatures.
		silence = (AudioClip)Resources.Load ("Sounds/silence", typeof(AudioClip));
		if (creatureName == "Magmabear") {
			++numOfInitializedCharacterSounds;
			clipList.Add (new KeyValuePair<string, AudioClip> ("Attack", (AudioClip)Resources.Load ("Sounds/Magmabear/tamadas", typeof(AudioClip))));
			clipList.Add (new KeyValuePair<string, AudioClip> ("Attack", (AudioClip)Resources.Load ("Sounds/Magmabear/tamadas_2", typeof(AudioClip))));
			clipList.Add (new KeyValuePair<string, AudioClip> ("Attack", (AudioClip)Resources.Load ("Sounds/Magmabear/tamadas_3", typeof(AudioClip))));

			clipList.Add (new KeyValuePair<string, AudioClip> ("Jump", (AudioClip)Resources.Load ("Sounds/Magmabear/ugras_1", typeof(AudioClip))));
			clipList.Add (new KeyValuePair<string, AudioClip> ("Jump", (AudioClip)Resources.Load ("Sounds/Magmabear/ugras_2", typeof(AudioClip))));
			clipList.Add (new KeyValuePair<string, AudioClip> ("Jump", (AudioClip)Resources.Load ("Sounds/Magmabear/ugras_3", typeof(AudioClip))));
		}
		if (creatureName == "Igor") {
			++numOfInitializedCharacterSounds;
			clipList.Add (new KeyValuePair<string, AudioClip> ("Attack", (AudioClip)Resources.Load ("Sounds/Igor/tamadas_1", typeof(AudioClip))));
			clipList.Add (new KeyValuePair<string, AudioClip> ("Attack", (AudioClip)Resources.Load ("Sounds/Igor/tamadas_2", typeof(AudioClip))));

			clipList.Add (new KeyValuePair<string, AudioClip> ("Jump", (AudioClip)Resources.Load ("Sounds/Igor/ugras", typeof(AudioClip))));
			clipList.Add (new KeyValuePair<string, AudioClip> ("Jump", (AudioClip)Resources.Load ("Sounds/Igor/ugras_2", typeof(AudioClip))));
			clipList.Add (new KeyValuePair<string, AudioClip> ("Jump", (AudioClip)Resources.Load ("Sounds/Igor/ugras_3", typeof(AudioClip))));
		}
		if (numOfInitializedCharacterSounds < 2) {
			Debug.LogWarning("Audiomanager: Not all characters' sounds have been initialized!");
		}
	}

	public static AudioClip getSound(string creatureName, string soundName) {
		List<KeyValuePair<string, AudioClip>> creatureAudio = null;
		if(creatureName == playerCreatureName)
			creatureAudio = playerAudio;
		if (creatureName == opponentCreatureName) {
			creatureAudio = opponentAudio;
		}
		// Nullptr return should be avoided, return audioclip for silence if no creature matches.
		if (creatureAudio == null)
		    return silence;

		List<AudioClip> possibleAudioClips = new List<AudioClip>();
		foreach (var clip in creatureAudio) {
			if(clip.Key == soundName) {
				possibleAudioClips.Add(clip.Value);
			}
		}

		if (possibleAudioClips.Count == 0)
			return null;

		// Dare not to enter the world of unity's random generation mechanism.
		int num = (int)Random.Range (0, possibleAudioClips.Count);
		return possibleAudioClips[num];
	}

}
