using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MeleeAttack : Skill
{
	public int damage = 20;
	protected override void Start()
	{
		base.Start();
		cooldownTime = 0.8f;
		castTime = 0.2f;
		Name = "Melee attack";
		Supertype = SkillSuperType.Attack;
		Subtype = SkillSubType.Physical;
		IsBasicAttack = true;

        // DemoAI
        aggrLevel = 1;
        period = 3;
    }

    public override float sense(DemoAI contr)
    {
        if (contr.creature.CurrentPlatform == contr.otherAI.creature.CurrentPlatform)
            return 1;
        return -2;
	}

	public override void overwrittenFire(Controller controller)
	{
		Owner.isAttacking = true;
		Creature enemy = Owner.World.GetOtherCreature(Owner);
		if (enemy.CurrentPlatform == Owner.CurrentPlatform)
		{
			enemy.TakeDamage(damage, Owner);
		}
	}

    public override void applyToState(ref GameState state, int pid)
    {
        if (state.players[pid].pos == state.players[GameState.otherPlayerId(pid)].pos)
        {
            var opid = GameState.otherPlayerId(pid);
            var otherplayer = state.players[opid];
            otherplayer.hp -= damage;
            state.players[opid] = otherplayer;
        }
    }

}