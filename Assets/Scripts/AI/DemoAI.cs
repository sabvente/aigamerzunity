﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class DemoAI : Controller
{
    List<Scenario> scenarios = new List<Scenario>();
    static float aggrWeight = 1f;
    static float freqWeight = 1f;
    static float senseWeight = 1.4f;

    public DemoAI otherAI; // needed?
    public float agressivity = 0f;
    public float learnspeed = 0f;
    public float foresight = 0f;

    new void Start()
    {
        base.Start();

        // add scenarios
        scenarios.Add(new LearnDodge());
        scenarios.Add(new LearnDefense());
		scenarios.Add(new LearnShootAhead());
        scenarios.Add(new RushForesight());
	}

    new void Update()
    {
        if (sequence.Count == 0 && (occupiedBy == null || occupiedBy.finished) && this.creature.CurrentPlatform != null
            && creature != null && otherAI.creature != null && !creature.Equals(null) && !otherAI.creature.Equals(null))
        {
            //Debug.Log(creature.Team.ToString() + " properties: " + agressivity + " " + learnspeed + " " + foresight);
            // random movement calc
            var applVals = new List<float>(creature.skills.Count);
            foreach (var skill in creature.skills)
            {
                var val =
                    //- skill.freq / (Time.time - skill.appliedSince) * freqWeight
                    //- skill.getCurrentCooldown() / skill.cooldownTime
                    - Math.Abs(skill.aggrLevel - agressivity) * aggrWeight
                    + skill.sense(this) * senseWeight;
                //if (skill.Name == "Approach")
                //if (creature.Team == Creature.Teams.Gaia)
                //    Debug.Log(creature.Team.ToString() + " - " + skill.Name + " aggr: " + (-Math.Abs(skill.aggrLevel - agressivity) * aggrWeight) + "; sense: " + (skill.sense(this) * senseWeight)
                //        + "; SUM: " + val);
                applVals.Add(val);
            }

            // eliminate negative elements
            for (int i = 0; i < applVals.Count; ++i) { applVals[i] = applVals[i] < 0 ? 0 : applVals[i]; }
            
            // skills - weighted random distribution
            float rand = UnityEngine.Random.Range(0f, applVals.Sum());
            int chosen = 0;
            for (int i = 0; i < applVals.Count; ++i)
            {
                if (rand < applVals[i])
                {
                    chosen = i;
                    break;
                }
                rand -= applVals[i];
            }
            Skill randomSkill = creature.skills[chosen];

            // scenarios - weighted random distribution
            applVals = scenarios.Select(x => x.applicability(this)).ToList();
            rand = UnityEngine.Random.Range(0f, applVals.Sum());
            Scenario randomScen = null;
            for (int i = 0; i < applVals.Count; ++i)
            {
                if (rand < applVals[i])
                {
                    randomScen = scenarios[i];
                    break;
                }
                rand -= applVals[i];
            }

            rand = UnityEngine.Random.Range(0f, 1f);
            if (rand < 2f * learnspeed * foresight && randomScen != null)
            {
                sequence.Clear();
                otherAI.sequence.Clear();
                randomScen.apply(this);
            }
            else
            {
                // defense predict
                if ( randomSkill.Name == "Defending" &&
                    ((creature.Team == Creature.Teams.Gaia && Scenario.stepDistance(this) > 1)
                    || creature.Team == Creature.Teams.Citadel && Scenario.stepDistance(this) == 0))
                    world.AIText("Predicts to defend", world.myCreature == creature);
                
                // fire skill
                InterruptSequence(randomSkill);
            }
        }
        base.Update();
	}
}
