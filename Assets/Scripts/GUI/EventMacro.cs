﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class EventMacro : MonoBehaviour
{
	public GameObject eventPrefab, skillPrefab;
	public GameObject eventPanel, skillPanel;
	public GameObject canvas, draggedSkill;
	private string draggedSkillName;
	public Sprite[] skillIcons;
	public Sprite emptySkillIcon;

	public Event[] events = {
								new Event() { SuperType = SkillSuperType.Idle, SubType = SkillSubType.Default, SkillName = "Idle" },
								new Event() { SuperType = SkillSuperType.Attack, SubType = SkillSubType.Default, SkillName = "Attack"},
								new Event() { SuperType = SkillSuperType.Attack, SubType = SkillSubType.Physical, SkillName = "MeleeAttack"},
								new Event() { SuperType = SkillSuperType.Move, SubType = SkillSubType.Default, SkillName = "Move" },
								new Event() { SuperType = SkillSuperType.Move, SubType = SkillSubType.Approach , SkillName = "MoveApproach"},
								new Event() { SuperType = SkillSuperType.Move, SubType = SkillSubType.Withdraw, SkillName = "MoveWitdraw" },
								new Event() { SuperType = SkillSuperType.Move, SubType = SkillSubType.Dodge, SkillName = "MoveDodge" }
							};
	public Dictionary<Event, string[]> macros = new Dictionary<Event, string[]>();
	private bool dragging = false;

	public class Event
	{
		public SkillSuperType SuperType { get; set; }
		public SkillSubType SubType { get; set; }
		public string SkillName { get; set; }
	}

	private void Start()
	{
		DontDestroyOnLoad(this);
		foreach (var item in events)
		{
			macros.Add(item, new string[5]);
		}

		for (int i = 0; i < events.Length; i++)
		{
			var a = (GameObject)Instantiate(eventPrefab, Vector3.zero, Quaternion.identity);
			a.transform.SetParent(eventPanel.transform, false);
			a.transform.localPosition = new Vector3(0, -10 - i * 60);
			a.transform.GetChild(0).GetComponent<Text>().text = events[i].SkillName;
			var savedEvent = events[i];
			for (int j = 1; j < 6; j++)
			{
				int t = j;
				var et = a.transform.GetChild(j).gameObject.AddComponent<EventTrigger>();
				et.delegates = new System.Collections.Generic.List<EventTrigger.Entry>();

				EventTrigger.Entry entry = new EventTrigger.Entry();
				entry.eventID = EventTriggerType.Drop;
				entry.callback = new EventTrigger.TriggerEvent();
				UnityEngine.Events.UnityAction<BaseEventData> callback =
					new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
					{
						if (dragging)
						{
							dragging = false;
							var rawImage = draggedSkill.GetComponent<RawImage>();
							rawImage.color = new Color(0, 0, 0, 0);
							Debug.Log(t);
							if (t == 5 || (6 - t - 2 < 5 && macros[savedEvent][6 - t - 2] != null))
							{
								et.gameObject.GetComponent<RawImage>().texture
									= draggedSkill.GetComponent<RawImage>().texture;
								macros[savedEvent][6 - t - 1] = draggedSkillName;
							}
							draggedSkillName = "";
						}
					});
				entry.callback.AddListener(callback);
				et.delegates.Add(entry);

				entry = new EventTrigger.Entry();
				entry.eventID = EventTriggerType.PointerClick;
				entry.callback = new EventTrigger.TriggerEvent();
				callback =
					new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
					{
						for (int k = t; k >= 0; k--)
						{
							et.gameObject.GetComponent<RawImage>().texture = null;
							macros[savedEvent][6 - t - 1] = null;
							et.gameObject.GetComponent<RawImage>().texture
								= emptySkillIcon.texture;
						}
					});
				entry.callback.AddListener(callback);
				et.delegates.Add(entry);
			}
		}

		// move callback
		var dragEvent = new EventTrigger.Entry();
		dragEvent.eventID = EventTriggerType.Drag;
		dragEvent.callback = new EventTrigger.TriggerEvent();
		dragEvent.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
			{
				var v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				draggedSkill.transform.position = new Vector3(v.x, v.y - 1, 0);
			}));

		// mouseup callback
		var moveEvent = new EventTrigger.Entry();
		moveEvent.eventID = EventTriggerType.PointerUp;
		moveEvent.callback = new EventTrigger.TriggerEvent();
		moveEvent.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
		{
			/*var rawImage = draggedSkill.GetComponent<RawImage>();
			rawImage.color = new Color(0, 0, 0, 0);
			dragging = false;*/
		}));

		var ba = GameObject.Find("BasicAttackPic");
		EventTrigger eventTrigger = ba.AddComponent<EventTrigger>();
		eventTrigger.delegates = new System.Collections.Generic.List<EventTrigger.Entry>();
		eventTrigger.delegates.Add(dragEvent);
		eventTrigger.delegates.Add(moveEvent);
		ba.AddComponent<EventTrigger>();
		EventTrigger.Entry etEntry = new EventTrigger.Entry();
		etEntry.eventID = EventTriggerType.PointerDown;
		etEntry.callback = new EventTrigger.TriggerEvent();
		etEntry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
		{
			var v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			draggedSkill.transform.position = new Vector3(v.x, v.y - 1, 0);
			var rawImage = draggedSkill.GetComponent<RawImage>();
			rawImage.color = Color.white;
			rawImage.texture = ba.GetComponent<RawImage>().texture;
			draggedSkillName = ba.GetComponent<UISkill>().Name;
			dragging = true;
		}));

		eventTrigger.delegates.Add(etEntry);

		var br = GameObject.Find("PoisonedDaggerPic");
		eventTrigger = br.AddComponent<EventTrigger>();
		eventTrigger.delegates = new System.Collections.Generic.List<EventTrigger.Entry>();
		eventTrigger.delegates.Add(dragEvent);
		eventTrigger.delegates.Add(moveEvent);
		br.AddComponent<EventTrigger>();
		etEntry = new EventTrigger.Entry();
		etEntry.eventID = EventTriggerType.PointerDown;
		etEntry.callback = new EventTrigger.TriggerEvent();
		etEntry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
		{
			var v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			draggedSkill.transform.position = new Vector3(v.x, v.y - 1, 0);
			var rawImage = draggedSkill.GetComponent<RawImage>();
			rawImage.color = Color.white;
			rawImage.texture = br.GetComponent<RawImage>().texture;
			draggedSkillName = br.GetComponent<UISkill>().Name;
			dragging = true;
		}));

		eventTrigger.delegates.Add(etEntry);



		var wd = GameObject.Find("WithdrawPic");
		eventTrigger = wd.AddComponent<EventTrigger>();
		eventTrigger.delegates = new System.Collections.Generic.List<EventTrigger.Entry>();
		eventTrigger.delegates.Add(dragEvent);
		eventTrigger.delegates.Add(moveEvent);
		ba.AddComponent<EventTrigger>();
		etEntry = new EventTrigger.Entry();
		etEntry.eventID = EventTriggerType.PointerDown;
		etEntry.callback = new EventTrigger.TriggerEvent();
		etEntry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
		{
			var v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			draggedSkill.transform.position = new Vector3(v.x, v.y - 1, 0);
			var rawImage = draggedSkill.GetComponent<RawImage>();
			rawImage.color = Color.white;
			rawImage.texture = wd.GetComponent<RawImage>().texture;
			draggedSkillName = wd.GetComponent<UISkill>().Name;
			dragging = true;
		}));

		eventTrigger.delegates.Add(etEntry);



		var ap = GameObject.Find("ApproachPic");
		eventTrigger = ap.AddComponent<EventTrigger>();
		eventTrigger.delegates = new System.Collections.Generic.List<EventTrigger.Entry>();
		eventTrigger.delegates.Add(dragEvent);
		eventTrigger.delegates.Add(moveEvent);
		ba.AddComponent<EventTrigger>();
		etEntry = new EventTrigger.Entry();
		etEntry.eventID = EventTriggerType.PointerDown;
		etEntry.callback = new EventTrigger.TriggerEvent();
		etEntry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
		{
			var v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			draggedSkill.transform.position = new Vector3(v.x, v.y - 1, 0);
			var rawImage = draggedSkill.GetComponent<RawImage>();
			rawImage.color = Color.white;
			rawImage.texture = ap.GetComponent<RawImage>().texture;
			draggedSkillName = ap.GetComponent<UISkill>().Name;
			dragging = true;
		}));

		eventTrigger.delegates.Add(etEntry);



		var d = GameObject.Find("DodgePic");
		eventTrigger = d.AddComponent<EventTrigger>();
		eventTrigger.delegates = new System.Collections.Generic.List<EventTrigger.Entry>();
		eventTrigger.delegates.Add(dragEvent);
		eventTrigger.delegates.Add(moveEvent);
		ba.AddComponent<EventTrigger>();
		etEntry = new EventTrigger.Entry();
		etEntry.eventID = EventTriggerType.PointerDown;
		etEntry.callback = new EventTrigger.TriggerEvent();
		etEntry.callback.AddListener(new UnityEngine.Events.UnityAction<BaseEventData>((e) =>
		{
			var v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			draggedSkill.transform.position = new Vector3(v.x, v.y - 1, 0);
			var rawImage = draggedSkill.GetComponent<RawImage>();
			rawImage.color = Color.white;
			rawImage.texture = d.GetComponent<RawImage>().texture;
			draggedSkillName = d.GetComponent<UISkill>().Name;
			dragging = true;
		}));

		eventTrigger.delegates.Add(etEntry);
	}
}
