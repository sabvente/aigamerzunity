﻿using UnityEngine;
using System.Collections;

public class SliderAIParams : MonoBehaviour {

	public int agressivityLeft, agressivityRight;
	public int learnLeft, learnRight;
	public int foresightLeft, foresightRight;

	public void Start()
	{
		DontDestroyOnLoad(this);
	}
}
