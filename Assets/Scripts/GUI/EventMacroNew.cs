﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class EventMacroNew : MonoBehaviour
{
	public GameObject skillIconGO;
	public GameObject eventPanel, skillPanel;
	public GameObject skillStoreGO;
	public GameObject creatureFactoryGO;

	public Event[] events = {
								new Event() { SuperType = SkillSuperType.Idle, SubType = SkillSubType.Default, SkillName = "Idle" },
								new Event() { SuperType = SkillSuperType.Attack, SubType = SkillSubType.Default, SkillName = "Attack"},
								new Event() { SuperType = SkillSuperType.Attack, SubType = SkillSubType.Physical, SkillName = "MeleeAttack"},
								new Event() { SuperType = SkillSuperType.Defense, SubType = SkillSubType.Default, SkillName = "Defense"},
								new Event() { SuperType = SkillSuperType.Move, SubType = SkillSubType.Default, SkillName = "Move" },
								new Event() { SuperType = SkillSuperType.Move, SubType = SkillSubType.Approach , SkillName = "MoveApproach"},
								new Event() { SuperType = SkillSuperType.Move, SubType = SkillSubType.Withdraw, SkillName = "MoveWitdraw" },
								new Event() { SuperType = SkillSuperType.Move, SubType = SkillSubType.Dodge, SkillName = "MoveDodge" }
							};
	public Dictionary<Event, string[]> macros = new Dictionary<Event, string[]>();
	private bool dragging = false;

	public class Event
	{
		public SkillSuperType SuperType { get; set; }
		public SkillSubType SubType { get; set; }
		public string SkillName { get; set; }
	}

	private void Start()
	{
		var creatureName = "Igor";
		var cs = GameObject.Find("SelectedCreature");
		if (cs != null)
			creatureName = cs.GetComponent<SelectedCreature>().selected;

		var factory = creatureFactoryGO.GetComponent<CreatureFactory>();
		var skillNames = factory.GetSoftAISkills(creatureName);
		var ssGO = (GameObject)Instantiate(skillStoreGO);
		SkillStore skillStore = ssGO.GetComponent<SkillStore>();
		foreach (var skillSprite in skillStore.skills)
		{
			if (skillNames.Contains(skillSprite.skillName))
			{
				string skillName = skillSprite.skillName;
				var skillIconClone = (GameObject)Instantiate(skillIconGO);
				skillIconClone.transform.SetParent(skillPanel.transform, false);
				//set sprite
				var img = skillIconClone.transform.Find("Image").GetComponent<Image>();
				img.sprite = skillSprite.sprite;
				//set skill name
				var text = skillIconClone.transform.Find("Text").GetComponent<Text>();
				text.text = skillName;
				//add click handler
				var btn = skillIconClone.GetComponent<Button>();
				btn.onClick.AddListener(() =>
				{
					//SelectSkill(skillIconClone, skillName);
				});
			}
		}
	}
}
