﻿using UnityEngine;
using System.Collections;

public class LearnDefense : LearnScenario
{
    public LearnDefense()
    {
        // fill learnContrSeq
        learnContrSeq.Add(5);
        learnContrSeq.Add(5);
        learnContrSeq.Add(5);
        learnContrSeq.Add(4);
        // fill learnEnemySeq
        learnEnemySeq.Add(2);
        learnEnemySeq.Add(0);
        learnEnemySeq.Add(5);
        // fill contrSeq
        //contrSeq.Add(5);
        learnContrSeq.Add(5);
        contrSeq.Add(1);
        contrSeq.Add(4);
        // fill enemySeq
        enemySeq.Add(2);
        enemySeq.Add(0);
        enemySeq.Add(5);
        textShowDur = 4f;
    }

    public override float applicability(DemoAI contr)
    {
        if (contr.creature.Team == Creature.Teams.Citadel && Scenario.stepDistance(contr) == 0)
            return (Time.time - appliedSince) / period;
        return 0;
    }

    public override void apply(DemoAI contr)
    {
        if (applicability(contr) <= 0) return;
        base.apply(contr);

        var ctodo = contrSeq;
        var etodo = enemySeq;
        if (learnt > 0) // learn
        {
            ctodo = learnContrSeq;
            etodo = learnEnemySeq;
            contr.world.AIText("Learns to defend", contr.world.myCreature == contr.creature, textShowDur);
        }
		else
		{
            contr.world.AIText("Defends (learnt)", contr.world.myCreature == contr.creature, textShowDur);
		}

        foreach (var s in ctodo)
            contr.EnqueueSequence(contr.creature.skills[s]);
        foreach (var s in etodo)
            contr.otherAI.EnqueueSequence(contr.otherAI.creature.skills[s]);
    }

}
