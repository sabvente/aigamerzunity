﻿using UnityEngine;
using System.Collections;

public class SelectedCreature : MonoBehaviour
{
	public string selected;
	void Awake()
	{
		if (FindObjectsOfType(this.GetType()).Length > 1)
		{
			DestroyImmediate(gameObject);
			return;
		}
		DontDestroyOnLoad(this);
	}
}
