using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class LeapAttack : Skill
{
	public int damage = 30;
	Platform targetPlatform;
	protected override void Start()
	{
		base.Start();
		cooldownTime = 30f;
		castTime = 0.2f;
		Name = "Leap attack";
		Supertype = SkillSuperType.Attack;
		Subtype = SkillSubType.Physical;
    }

	public override void overwrittenFire(Controller controller)
	{
		if (CanFire() && Owner.CurrentPlatform != null)
		{
			var enemy = Owner.World.GetOtherCreature(Owner);
			targetPlatform = enemy.CurrentPlatform;
			targetPlatform = targetPlatform ?? enemy.lastPlatform;
			if (targetPlatform == null)
				return;

			//on the same platform
			if (targetPlatform == Owner.CurrentPlatform)
			{
				//TODO: Jump up or smthing
				enemy.TakeDamage(damage, Owner);
				PlayAnimation();
				base.Fire(controller);
			}
			else
			{
				bool canJump = Owner.JumpTo(targetPlatform);
				if (canJump)
				{
					targetPlatform.SteppedOn += DamageEnemy;
					base.Fire(controller);
				}
			}
		}
	}

	void DamageEnemy(Creature creature)
	{
		//the owner arrived to the platform
		if (creature == Owner)
		{
			targetPlatform.SteppedOn -= DamageEnemy;
			var otherC = targetPlatform.GetOtherCreature(Owner);
			if (otherC != null)
			{
				otherC.TakeDamage(damage, Owner);
				PlayAnimation();
			}
		}
	}

	void PlayAnimation()
	{
		//TODO: PlayAnimation
	}
}
