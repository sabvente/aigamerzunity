using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Macro : Skill
{
	public List<Skill> skills = new List<Skill>();

	protected override void Start()
	{
		base.Start();
		if (skills.Count > 0)
		{
			cooldownTime = skills.Select(x => x.cooldownTime).Max();
			castTime = skills.Select(x => x.cooldownTime).Sum();
		}
		else
			cooldownTime = 0f;
	}

	void addSkill(Skill s)
	{
		skills.Add(s);
		if (s.cooldownTime > cooldownTime)
			cooldownTime = s.cooldownTime;
		castTime += s.castTime;
	}

	// don't work like Skill.Fire -> this one just add Skills to sequence
	public override void overwrittenFire(Controller controller)
	{
		if (true) // CanFire()
		{
			base.Fire(controller);
			Debug.Log("CAN BE FIRED " + skills.Count);
			foreach (var skill in skills)
			{
				Debug.Log("SKILL ADDED: " + skill.Name);
				controller.sequence.Enqueue(skill);
			}
		}
	}
}

