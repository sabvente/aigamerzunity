using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class UseHealthPotion : Skill
{
	protected override void Start()
	{
		base.Start();
		cooldownTime = 30f;
		castTime = 0.2f;
		Name = "Use Health Potion";
		Supertype = SkillSuperType.Potion;
		Subtype = SkillSubType.Heal;
	}

	public override void overwrittenFire(Controller controller)
	{
		if (CanFire() && Owner.CurrentPlatform != null)
		{
			Owner.Heal(40);
			base.Fire(controller);
		}
	}
}
