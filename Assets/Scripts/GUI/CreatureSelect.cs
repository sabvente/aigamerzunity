﻿using UnityEngine;
using System.Collections;

public class CreatureSelect : MonoBehaviour {

	public void CreatureSelected(string creatureName)
	{
		GameObject.Find("SelectedCreature").GetComponent<SelectedCreature>().selected = creatureName;
	}
}
